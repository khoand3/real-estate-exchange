'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_PROJECT = 2;
const gCOL_ACREAGE = 3;
const gCOL_DATE_CREATE = 4;
const gCOL_DATE_UPDATE = 5;
const gCOL_DESCRIPTION = 6;
const gCOL_EDIT = 7;
const gCOL_DELETE = 8;
const gDATA_COLS = [
  'id',
  'name',
  'project.name',
  'acreage',
  'dateCreate',
  'dateUpdate',
  'description',
];
var gTable = $('#master-layout-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_NAME] },
    { data: gDATA_COLS[gCOL_PROJECT] },
    { data: gDATA_COLS[gCOL_ACREAGE] },
    { data: gDATA_COLS[gCOL_DATE_CREATE] },
    { data: gDATA_COLS[gCOL_DATE_UPDATE] },
    { data: gDATA_COLS[gCOL_DESCRIPTION] },
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
  ],
  columnDefs: [
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
});
//bien toan cuc luu gia tri id
var gProjectId = 0;
var gMasterLayoutId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gMasterLayoutId = 0;
    emptyDataInput();
    $('#select-project').prop('disabled', false);
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi chon project
  $('#select-project').on('change', function () {
    onSelectProjectChange(this);
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#master-layout-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#master-layout-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/master-layouts', loadDataToTable);
  $.get('http://localhost:8080/projects', loadDataToSelectProject);
}
//ham xu ly su kien khi chon tinh thanh
function onSelectProjectChange(pElement) {
  gProjectId = $(pElement).val();
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gMasterLayoutId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gMasterLayoutId = vCurrentRowData.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#select-project').prop('disabled', 'disabled');
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gMasterLayoutId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
function getInputData() {
  var vDataObj = {
    name: '',
    description: '',
    acreage: '',
  };

  vDataObj.name = $('#input-name').val().trim();
  vDataObj.description = $('#txtarea-description').val().trim();
  vDataObj.acreage = $('#input-acreage').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.name === '') {
      vResult = false;
      throw 'Vui lòng nhập tên mặt bằng tầng';
    }
    if ($('#select-project').val() === '-1') {
      vResult = false;
      throw 'Vui lòng chọn dự án';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/projects/' + gProjectId + '/master-layouts',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/master-layouts', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/master-layouts/' + gMasterLayoutId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/master-layouts', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/master-layouts/' + gMasterLayoutId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/master-layouts', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#input-name').val('');
  $('#select-project').val('-1');
  $('#txtarea-description').val('');
  $('#input-acreage').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#input-name').val(pDataObj.name);
  $('#select-project').val(pDataObj.project.id);
  $('#txtarea-description').val(pDataObj.description);
  $('#input-acreage').val(pDataObj.acreage);
}
//ham load du lieu len select project tren modal
function loadDataToSelectProject(pData) {
  var vSelectProjectElement = $('#select-project');
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = `<option value="${pData[bI].id}">${pData[bI].name}</option>`;
    vSelectProjectElement.append(bOption);
  }
}
