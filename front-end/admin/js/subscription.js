'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_ID = 0;
const gCOL_USER = 1;
const gCOL_ENDPOINT = 2;
const gCOL_PUBLIC_KEY = 3;
const gCOL_AUTHENTICATION_TOKEN = 4;
const gCOL_CONTENT_ENCODING = 5;
const gCOL_EDIT = 6;
const gCOL_DELETE = 7;
const gDATA_COLS = [
  'id',
  'user',
  'endpoint',
  'publickey',
  'authenticationToken',
  'contentEncoding',
];
var gTable = $('#subscription-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_USER] },
    { data: gDATA_COLS[gCOL_ENDPOINT] },
    { data: gDATA_COLS[gCOL_PUBLIC_KEY] },
    { data: gDATA_COLS[gCOL_AUTHENTICATION_TOKEN] },
    { data: gDATA_COLS[gCOL_CONTENT_ENCODING] },
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
  ],
  columnDefs: [
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
});
//bien toan cuc luu gia tri id
var gSubscriptionId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gSubscriptionId = 0;
    emptyDataInput();
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#subscription-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#subscription-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/employees', loadDataToSelectUser);
  $.get('http://localhost:8080/subscriptions', loadDataToTable);
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gSubscriptionId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gSubscriptionId = vCurrentRowData.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gSubscriptionId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
//ham load data len select user
function loadDataToSelectUser(pData) {
  var vSelectUserElement = $('#select-user');
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = `<option value=${pData[bI].id}>${pData[bI].username}</option>`;
    vSelectUserElement.append(bOption);
  }
}
function getInputData() {
  var vDataObj = {
    user: '',
    endpoint: '',
    publickey: '',
    authenticationToken: '',
    contentEncoding: '',
  };

  vDataObj.user = $('#select-user').val();
  vDataObj.endpoint = $('#input-endpoint').val().trim();
  vDataObj.publickey = $('#input-public-key').val().trim();
  vDataObj.authenticationToken = $('#input-authentication-token').val().trim();
  vDataObj.contentEncoding = $('#input-content-encoding').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.user === '0') {
      vResult = false;
      throw 'Vui lòng chọn user';
    }
    if (pDataObj.endpoint === '') {
      vResult = false;
      throw 'Vui lòng nhập endpoint';
    }
    if (pDataObj.publickey === '') {
      vResult = false;
      throw 'Vui lòng nhập public key';
    }
    if (pDataObj.authenticationToken === '') {
      vResult = false;
      throw 'Vui lòng nhập authentication token';
    }
    if (pDataObj.contentEncoding === '') {
      vResult = false;
      throw 'Vui lòng nhập content encoding';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/subscriptions',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/subscriptions', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/subscriptions/' + gSubscriptionId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/subscriptions', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/subscriptions/' + gSubscriptionId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/subscriptions', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#select-user').val('0');
  $('#input-endpoint').val('');
  $('#input-public-key').val('');
  $('#input-authentication-token').val('');
  $('#input-content-encoding').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#select-user').val(parseInt(pDataObj.user));
  $('#input-endpoint').val(pDataObj.endpoint);
  $('#input-public-key').val(pDataObj.publickey);
  $('#input-authentication-token').val(pDataObj.authenticationToken);
  $('#input-content-encoding').val(pDataObj.contentEncoding);
}
