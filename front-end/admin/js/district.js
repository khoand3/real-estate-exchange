'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_PROJECT = 0;
const gCOL_REAL_ESTATE = 1;
const gCOL_ID = 2;
const gCOL_NAME = 3;
const G_COL_PREFIX = 4;
const gCOL_PROVINCE = 5;
const gCOL_EDIT = 6;
const gCOL_DELETE = 7;
const gDATA_COLS = [
  'project',
  'real-estate',
  'districtId',
  'districtName',
  'districtPrefix',
  'provinceName',
];
var gTable = $('#district-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_PROJECT] },
    { data: gDATA_COLS[gCOL_REAL_ESTATE] },
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_NAME] },
    { data: gDATA_COLS[G_COL_PREFIX] },
    { data: gDATA_COLS[gCOL_PROVINCE] },
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
  ],
  columnDefs: [
    {
      targets: gCOL_PROJECT,
      className: 'text-center',
      defaultContent: `<button title="Dự án xây dựng" class="btn btn-secondary btn-project-detail" style="cursor: pointer">Dự án xây dựng</button>`,
    },
    {
      targets: gCOL_REAL_ESTATE,
      className: 'text-center',
      defaultContent: `<button title="Thông tin BĐS" class="btn btn-secondary btn-real-estate-detail" style="cursor: pointer">Thông tin BĐS</button>`,
    },
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
  autoWidth: false,
});
//bien toan cuc luu gia tri id
var gProvinceId = 0;
var gDistrictId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gDistrictId = 0;
    emptyDataInput();
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi chon tinh thanh
  $('#select-province').on('change', function () {
    onSelectProvinceChange(this);
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#district-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#district-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/districts-provinces', loadDataToTable);
  $.get('http://localhost:8080/provinces', loadDataToSelectProvince);
}
//ham xu ly su kien khi chon tinh thanh
function onSelectProvinceChange(pElement) {
  gProvinceId = $(pElement).val();
  console.log(gProvinceId);
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gDistrictId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gProvinceId = vCurrentRowData.provinceId;
  gDistrictId = vCurrentRowData.districtId;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gProvinceId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
function getInputData() {
  var vDataObj = {
    name: '',
    prefix: '',
  };

  vDataObj.name = $('#input-name').val().trim();
  vDataObj.prefix = $('#select-prefix').val();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if ($('#select-province').val() === '0') {
      vResult = false;
      throw 'Vui lòng chọn tỉnh thành';
    }
    if (pDataObj.prefix === '0') {
      vResult = false;
      throw 'Vui lòng chọn tiếp đầu ngữ';
    }
    if (pDataObj.name === '') {
      vResult = false;
      throw 'Vui lòng nhập tên quận huyện';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/provinces/' + gProvinceId + '/districts',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/districts-provinces', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/districts/' + gDistrictId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/districts-provinces', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/districts/' + gDistrictId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/districts-provinces', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#select-province').val('0');
  $('#select-prefix').val('0');
  $('#input-name').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#select-province').val(pDataObj.provinceId);
  $('#select-prefix').val(pDataObj.districtPrefix);
  $('#input-name').val(pDataObj.districtName);
}
//ham load du lieu len select province tren modal
function loadDataToSelectProvince(pData) {
  var vSelectProvinceElement = $('#select-province');
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = `<option value="${pData[bI].id}">${pData[bI].name}</option>`;
    vSelectProvinceElement.append(bOption);
  }
}
