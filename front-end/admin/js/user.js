'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_EDIT = 0;
const gCOL_DELETE = 1;
const gCOL_ID = 2;
const gCOL_LAST_NAME = 3;
const gCOL_FIRST_NAME = 4;
const gCOL_TITLE = 5;
const gCOL_TITLE_OF_COURTESY = 6;
const gCOL_BIRTH_DATE = 7;
const gCOL_HIRE_DATE = 8;
const gCOL_ADDRESS = 9;
const gCOL_CITY = 10;
const gCOL_COUNTRY = 11;
const gCOL_PHONE = 12;
const gCOL_USERNAME = 13;
const gCOL_PASSWORD = 14;
const gCOL_EMAIL = 15;
const gCOL_ACTIVATED = 16;
const gCOL_USER_LEVEL = 17;
const gDATA_COLS = [
  'edit',
  'delete',
  'id',
  'lastName',
  'firstName',
  'title',
  'titleOfCourtesy',
  'birthDate',
  'hireDate',
  'address',
  'city',
  'country',
  'homePhone',
  'username',
  'password',
  'email',
  'activated',
  'userLevel',
];
var gTable = $('#employee-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_LAST_NAME] },
    { data: gDATA_COLS[gCOL_FIRST_NAME] },
    { data: gDATA_COLS[gCOL_TITLE] },
    { data: gDATA_COLS[gCOL_TITLE_OF_COURTESY] },
    { data: gDATA_COLS[gCOL_BIRTH_DATE] },
    { data: gDATA_COLS[gCOL_HIRE_DATE] },
    { data: gDATA_COLS[gCOL_ADDRESS] },
    { data: gDATA_COLS[gCOL_CITY] },
    { data: gDATA_COLS[gCOL_COUNTRY] },
    { data: gDATA_COLS[gCOL_PHONE] },
    { data: gDATA_COLS[gCOL_USERNAME] },
    { data: gDATA_COLS[gCOL_PASSWORD] },
    { data: gDATA_COLS[gCOL_EMAIL] },
    { data: gDATA_COLS[gCOL_ACTIVATED] },
    { data: gDATA_COLS[gCOL_USER_LEVEL] },
  ],
  columnDefs: [
    {
      targets: gCOL_ACTIVATED,
      className: 'text-center',
      render: function (data, type, row) {
        if (type === 'display') {
          if (data == 'Y') {
            return '<input type="checkbox" checked disabled>';
          } else {
            return '<input type="checkbox" disabled>';
          }
        }
        return data;
      },
    },
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
  autoWidth: false,
});
//bien toan cuc luu gia tri id
var gEmployeeId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gEmployeeId = 0;
    emptyDataInput();
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#employee-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#employee-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/employees', loadDataToTable);
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gEmployeeId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gEmployeeId = vCurrentRowData.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gEmployeeId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
function getInputData() {
  var vDataObj = {
    lastName: '',
    firstName: '',
    title: '',
    titleOfCourtesy: '',
    birthDate: '',
    hireDate: '',
    address: '',
    city: '',
    country: '',
    homePhone: '',
    username: '',
    password: '',
    email: '',
    activated: '',
    userLevel: '',
    notes: '',
  };

  vDataObj.lastName = $('#input-last-name').val().trim();
  vDataObj.firstName = $('#input-first-name').val().trim();
  vDataObj.title = $('#input-title').val().trim();
  vDataObj.titleOfCourtesy = $('#input-title-of-courtesy').val().trim();
  vDataObj.birthDate = $('#input-birth-date').val().trim();
  vDataObj.hireDate = $('#input-hire-date').val().trim();
  vDataObj.address = $('#input-address').val().trim();
  vDataObj.city = $('#input-city').val().trim();
  vDataObj.country = $('#input-country').val().trim();
  vDataObj.homePhone = $('#input-mobile').val().trim();
  vDataObj.username = $('#input-username').val().trim();
  vDataObj.password = $('#input-password').val().trim();
  vDataObj.email = $('#input-email').val().trim();
  var vCheckbox = $('#checkbox-activated');
  if (vCheckbox.is(':checked')) {
    vDataObj.activated = 'Y';
  } else {
    vDataObj.activated = 'N';
  }
  console.log(vDataObj.activated);
  vDataObj.userLevel = $('#select-user-level').val().trim();
  vDataObj.notes = $('#txtarea-note').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.lastName === '') {
      vResult = false;
      throw 'Vui lòng nhập họ';
    }
    if (pDataObj.firstName === '') {
      vResult = false;
      throw 'Vui lòng nhập tên';
    }
    if (pDataObj.homePhone === '') {
      vResult = false;
      throw 'Vui lòng nhập số điện thoại';
    }
    if (pDataObj.username === '') {
      vResult = false;
      throw 'Vui lòng nhập username';
    }
    if (pDataObj.password === '') {
      vResult = false;
      throw 'Vui lòng nhập password';
    }
    if (pDataObj.homePhone === '') {
      vResult = false;
      throw 'Vui lòng nhập số điện thoại';
    }
    if (isNaN(pDataObj.homePhone)) {
      vResult = false;
      throw 'Số điện thoại phải là dãy số';
    }
    if (validateEmail(pDataObj.email) === false) {
      vResult = false;
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham kiem tra email
function validateEmail(pEmail) {
  var vResultEmail = true;
  var validRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  try {
    if (pEmail.length < 3) {
      vResultEmail = false;
      throw 'Vui lòng nhập email';
    } else {
      if (!pEmail.match(validRegex)) {
        vResultEmail = false;
        throw 'Email không hợp lệ';
      }
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResultEmail;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/employees',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/employees', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/employees/' + gEmployeeId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/employees', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/employees/' + gEmployeeId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/employees', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#input-last-name').val('');
  $('#input-first-name').val('');
  $('#input-title').val('');
  $('#input-title-of-courtesy').val('');
  $('#input-birth-date').val('');
  $('#input-hire-date').val('');
  $('#input-address').val('');
  $('#input-city').val('');
  $('#input-country').val('');
  $('#input-mobile').val('');
  $('#input-username').val('');
  $('#input-password').val('');
  $('#input-email').val('');
  $('#checkbox-activated').prop('checked', false);
  $('#select-user-level').val('1');
  $('#txtarea-note').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#input-last-name').val(pDataObj.lastName);
  $('#input-first-name').val(pDataObj.firstName);
  $('#input-title').val(pDataObj.title);
  $('#input-title-of-courtesy').val(pDataObj.titleOfCourtesy);
  $('#input-birth-date').val(formatDate(pDataObj.birthDate));
  $('#input-hire-date').val(formatDate(pDataObj.hireDate));
  $('#input-address').val(pDataObj.address);
  $('#input-city').val(pDataObj.city);
  $('#input-country').val(pDataObj.country);
  $('#input-mobile').val(pDataObj.homePhone);
  $('#input-username').val(pDataObj.username);
  $('#input-password').val(pDataObj.password);
  $('#input-email').val(pDataObj.email);
  console.log(pDataObj.activated);
  if (pDataObj.activated == 'Y') {
    $('#checkbox-activated').prop('checked', true);
  } else {
    $('#checkbox-activated').prop('checked', false);
  }
  $('#select-user-level').val(pDataObj.userLevel);
  $('#txtarea-note').val(pDataObj.notes);
}
//ham format date
function formatDate(pDate) {
  if (pDate !== null) {
    var splitDate = pDate.split('-');
    return splitDate[2] + '-' + splitDate[1] + '-' + splitDate[0];
  }
  return;
}
