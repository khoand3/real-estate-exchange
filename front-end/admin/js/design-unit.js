'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_DESCRIPTION = 2;
const gCOL_PROJECTS = 3;
const gCOL_ADDRESS = 4;
const gCOL_PHONE = 5;
const gCOL_PHONE2 = 6;
const gCOL_FAX = 7;
const gCOL_EMAIL = 8;
const gCOL_WEBSITE = 9;
const gCOL_NOTE = 10;
const gCOL_EDIT = 11;
const gCOL_DELETE = 12;
const gDATA_COLS = [
  'id',
  'name',
  'description',
  'projects',
  'address',
  'phone',
  'phone2',
  'fax',
  'email',
  'website',
  'note',
];
var gTable = $('#design-unit-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_NAME] },
    { data: gDATA_COLS[gCOL_DESCRIPTION] },
    { data: gDATA_COLS[gCOL_PROJECTS] },
    { data: gDATA_COLS[gCOL_ADDRESS] },
    { data: gDATA_COLS[gCOL_PHONE] },
    { data: gDATA_COLS[gCOL_PHONE2] },
    { data: gDATA_COLS[gCOL_FAX] },
    { data: gDATA_COLS[gCOL_EMAIL] },
    { data: gDATA_COLS[gCOL_WEBSITE] },
    { data: gDATA_COLS[gCOL_NOTE] },
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
  ],
  columnDefs: [
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
});
//bien toan cuc luu gia tri id
var gDesignUnitId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gDesignUnitId = 0;
    emptyDataInput();
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#design-unit-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#design-unit-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/design-units', loadDataToTable);
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gDesignUnitId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gDesignUnitId = vCurrentRowData.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gDesignUnitId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}

function getInputData() {
  var vDataObj = {
    name: '',
    description: '',
    projects: '',
    address: '',
    phone: '',
    phone2: '',
    fax: '',
    email: '',
    website: '',
    note: '',
  };

  vDataObj.name = $('#input-name').val().trim();
  vDataObj.description = $('#txtarea-description').val().trim();
  vDataObj.projects = $('#txtarea-projects').val().trim();
  vDataObj.address = $('#input-address').val().trim();
  vDataObj.phone = $('#input-phone').val().trim();
  vDataObj.phone2 = $('#input-phone2').val().trim();
  vDataObj.fax = $('#input-fax').val().trim();
  vDataObj.email = $('#input-email').val().trim();
  vDataObj.website = $('#input-website').val().trim();
  vDataObj.note = $('#txtarea-note').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.name === '') {
      vResult = false;
      throw 'Tên đơn vị thiết kế không được để trống';
    }
    if (validateEmail(pDataObj.email) === false) {
      vResult = false;
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham kiem tra email
function validateEmail(pEmail) {
  var vResultEmail = true;
  var validRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  try {
    if (!pEmail.match(validRegex)) {
      vResultEmail = false;
      throw 'Email không hợp lệ';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResultEmail;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/design-units',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/design-units', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/design-units/' + gDesignUnitId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/design-units', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/design-units/' + gDesignUnitId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/design-units', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#input-name').val('');
  $('#txtarea-description').val('');
  $('#txtarea-projects').val('');
  $('#input-address').val('');
  $('#input-phone').val('');
  $('#input-phone2').val('');
  $('#input-fax').val('');
  $('#input-email').val('');
  $('#input-website').val('');
  $('#txtarea-note').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#input-name').val(pDataObj.name);
  $('#txtarea-description').val(pDataObj.description);
  $('#txtarea-projects').val(pDataObj.projects);
  $('#input-address').val(pDataObj.address);
  $('#input-phone').val(pDataObj.phone);
  $('#input-phone2').val(pDataObj.phone2);
  $('#input-fax').val(pDataObj.fax);
  $('#input-email').val(pDataObj.email);
  $('#input-website').val(pDataObj.website);
  $('#txtarea-note').val(pDataObj.note);
}
