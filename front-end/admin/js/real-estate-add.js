'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//bien toan cuc luu gia tri id
var gRealEstateId = 0;
var gProjectId = -1;
var gDistrictId = 0;
var gProvinceId = 0;
var gCustomerId = 0;
//bien toan cuc luu phan tu select
const gSelectProjectElement = $('#select-project');
const gSelectProvinceElement = $('#select-province');
const gSelectDistrictElement = $('#select-district');
const gSelectCustomerElement = $('#select-customer');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien cho nut cancel
  $('#btn-cancel').on('click', function () {
    window.location.href = './real-estate.html';
  });
  //gan ham xu ly su kien khi chon tinh thanh
  gSelectProvinceElement.on('change', function () {
    onSelectProvinceChange(this);
  });
  //gan ham xu ly su kien khi chon quan huyen
  gSelectDistrictElement.on('change', function () {
    gDistrictId = $(this).val();
  });
  //gan ham xu ly su kien khi chon du an
  gSelectProjectElement.on('change', function () {
    gProjectId = $(this).val();
  });
  //gan ham xu ly su kien khi chon khach hang
  gSelectCustomerElement.on('change', function () {
    gCustomerId = $(this).val();
  });
  //gan ham xu ly su kien khi click nut save
  $('#btn-save').on('click', function () {
    onBtnSaveClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  callApiToGetAll('provinces', gSelectProvinceElement);
  callApiToGetAll('projects', gSelectProjectElement);
  callApiToGetAll('customers', gSelectCustomerElement);
  //tạo đối tượng url để truy vấn tham số
  var vUrlObject = new URL(window.location.href);
  // đưa tham số real estate id sang bằng query string
  gRealEstateId = vUrlObject.searchParams.get('id');
  if (gRealEstateId != null) {
    callApiGetDataById(gRealEstateId);
  } else {
    gRealEstateId = 0;
  }
}
//ham xu ly su kien khi chon tinh thanh
function onSelectProvinceChange(pElement) {
  gProvinceId = $(pElement).val();
  $.get(
    'http://localhost:8080/provinces/' + gProvinceId + '/districts',
    loadDataToSelectDistrict
  );
}
//ham xu ly su kien khi click nut save
function onBtnSaveClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gRealEstateId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham goi api de lay tat ca thong tin
function callApiToGetAll(pEntityName, pElement) {
  $.ajax({
    url: 'http://localhost:8080/' + pEntityName,
    method: 'GET',
    async: false,
    success: function (res) {
      loadDataToSelect(pElement, res);
    },
    error: function (err) {
      console.log(err);
    },
  });
}
function getInputData() {
  var vDataObj = {
    title: '',
    type: '',
    request: '',
    address: '',
    price: '',
    priceMin: '',
    priceTime: '',
    acreage: '',
    direction: '',
    totalFloors: '',
    numberFloors: '',
    bath: '',
    apartCode: '',
    wallArea: '',
    bedroom: '',
    balcony: '',
    landscapeView: '',
    apartLoca: '',
    apartType: '',
    furnitureType: '',
    priceRent: '',
    returnRate: '',
    legalDoc: '',
    description: '',
    widthY: '',
    longX: '',
    streetHouse: '',
    fsbo: '',
    viewNum: '',
    updateBy: '',
    shape: '',
    distance2facade: '',
    adjacent_facade_num: '',
    adjacentRoad: '',
    alley_min_width: '',
    adjacent_alley_min_width: '',
    factor: '',
    structure: '',
    dtsxd: '',
    clcl: '',
    ctxd_price: '',
    ctxd_value: '',
    photo: '',
    lat: '',
    lng: '',
  };
  vDataObj.title = $('#input-title').val().trim();
  vDataObj.type = $('#select-type').val();
  vDataObj.request = $('#select-request').val().trim();
  vDataObj.address = $('#input-address').val().trim();
  vDataObj.price = $('#input-price').val().trim();
  vDataObj.priceMin = $('#input-priceMin').val().trim();
  vDataObj.priceTime = $('#input-priceTime').val().trim();
  vDataObj.acreage = $('#input-acreage').val().trim();
  vDataObj.direction = $('#input-direction').val().trim();
  vDataObj.totalFloors = $('#input-totalFloors').val().trim();

  vDataObj.numberFloors = $('#input-numberFloors').val().trim();
  vDataObj.bath = $('#input-bath').val().trim();
  vDataObj.apartCode = $('#input-apartCode').val().trim();
  vDataObj.wallArea = $('#input-wallArea').val().trim();
  vDataObj.bedroom = $('#input-bedroom').val().trim();
  vDataObj.balcony = $('#input-balcony').val().trim();
  vDataObj.landscapeView = $('#input-landscapeView').val().trim();
  vDataObj.apartLoca = $('#input-apartLoca').val().trim();
  vDataObj.apartType = $('#input-apartType').val().trim();
  vDataObj.furnitureType = $('#input-furnitureType').val().trim();
  vDataObj.priceRent = $('#input-priceRent').val().trim();

  vDataObj.returnRate = $('#input-returnRate').val().trim();
  vDataObj.legalDoc = $('#input-legalDoc').val().trim();
  vDataObj.description = $('#input-description').val().trim();
  vDataObj.widthY = $('#input-widthY').val().trim();
  vDataObj.longX = $('#input-longX').val().trim();
  vDataObj.streetHouse = $('#input-streetHouse').val().trim();
  vDataObj.fsbo = $('#input-fsbo').val().trim();
  vDataObj.viewNum = $('#input-viewNum').val().trim();
  vDataObj.updateBy = $('#input-updateBy').val().trim();

  vDataObj.shape = $('#input-shape').val().trim();
  vDataObj.distance2facade = $('#input-distance2facade').val().trim();
  vDataObj.adjacent_facade_num = $('#input-adjacent_facade_num').val().trim();
  vDataObj.adjacentRoad = $('#input-adjacentRoad').val().trim();
  vDataObj.alley_min_width = $('#input-alley_min_width').val().trim();
  vDataObj.adjacent_alley_min_width = $('#input-adjacent_alley_min_width')
    .val()
    .trim();
  vDataObj.factor = $('#input-factor').val().trim();
  vDataObj.structure = $('#input-structure').val().trim();
  vDataObj.dtsxd = $('#input-dtsxd').val().trim();
  vDataObj.clcl = $('#input-clcl').val().trim();
  vDataObj.ctxd_price = $('#input-ctxd_price').val().trim();

  vDataObj.ctxd_value = $('#input-ctxd_value').val().trim();
  vDataObj.photo = $('#input-photo').val().trim();
  vDataObj.lat = $('#input-lat').val().trim();
  vDataObj.lng = $('#input-lng').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.address === '') {
      vResult = false;
      throw 'Vui lòng nhập địa chỉ';
    }
    if (gSelectProvinceElement.val() === '0') {
      vResult = false;
      throw 'Vui lòng chọn tỉnh thành';
    }
    if (gSelectDistrictElement.val() === '0') {
      vResult = false;
      throw 'Vui lòng chọn quận huyện';
    }
    if (gSelectProjectElement.val() === '-1') {
      vResult = false;
      throw 'Vui lòng chọn dự án';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url:
      gCustomerId === 0
        ? `http://localhost:8080/real-estates/${gProvinceId}/${gDistrictId}/${gProjectId}`
        : `http://localhost:8080/real-estates/${gProvinceId}/${gDistrictId}/${gProjectId}/${gCustomerId}/`,
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      alert('Thêm mới thành công');
      window.location.href = './real-estate.html';
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url:
      gCustomerId === 0
        ? `http://localhost:8080/real-estates/${gRealEstateId}/${gProvinceId}/${gDistrictId}/${gProjectId}`
        : `http://localhost:8080/real-estates/${gRealEstateId}/${gProvinceId}/${gDistrictId}/${gProjectId}/${gCustomerId}/`,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(this.url);
      console.log(response);
      alert('Cập nhật thành công');
      window.location.href = './real-estate.html';
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham load du lieu real estate theo id
function callApiGetDataById(pId) {
  $.ajax({
    url: 'http://localhost:8080/real-estates/' + pId,
    method: 'GET',
    success: function (res) {
      console.log(res);
      loadDataToInput(res);
    },
    error: function (err) {
      console.log(err);
    },
  });
}
//ham load du lieu len select
function loadDataToSelect(pElement, pData) {
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = $(
      `<option value="${pData[bI].id}">${pData[bI].name}</option>`
    );
    pElement.append(bOption);
  }
}
//ham load du lieu len select district
function loadDataToSelectDistrict(pData) {
  gSelectDistrictElement.html('');
  gSelectDistrictElement.append(
    `<option value="0">--Chọn quận huyện--</option>`
  );
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = `<option value="${pData[bI].id}">${pData[bI].name}</option>`;
    gSelectDistrictElement.append(bOption);
  }
}
//ham load du lieu len input
function loadDataToInput(pDataObj) {
  gSelectDistrictElement.html('');
  gSelectDistrictElement.append(
    `<option value=${pDataObj.district.id}>${pDataObj.district.name}</option>`
  );
  gDistrictId = pDataObj.district.id;
  gSelectProvinceElement.val(pDataObj.district.province.id);
  gProvinceId = pDataObj.district.province.id;
  gSelectProjectElement.val(pDataObj.project.id);
  gProjectId = pDataObj.project.id;
  if (pDataObj.customer != null) {
    gSelectCustomerElement.val(pDataObj.customer.id);
    gCustomerId = pDataObj.customer.id;
  }
  $('#input-title').val(pDataObj.title);
  $('#select-type').val(pDataObj.type);
  $('#select-request').val(pDataObj.request);
  $('#input-address').val(pDataObj.address);
  $('#input-price').val(pDataObj.price);
  $('#input-priceMin').val(pDataObj.priceMin);
  $('#input-priceTime').val(pDataObj.priceTime);
  $('#input-acreage').val(pDataObj.acreage);
  $('#input-direction').val(pDataObj.direction);
  $('#input-totalFloors').val(pDataObj.totalFloors);

  $('#input-numberFloors').val(pDataObj.numberFloors);
  $('#input-apartCode').val(pDataObj.apartCode);
  $('#input-wallArea').val(pDataObj.wallArea);
  $('#input-bedroom').val(pDataObj.bedroom);
  $('#input-bath').val(pDataObj.bath);
  $('#input-balcony').val(pDataObj.balcony);
  $('#input-landscapeView').val(pDataObj.landscapeView);
  $('#input-apartLoca').val(pDataObj.apartLoca);
  $('#input-apartType').val(pDataObj.apartType);
  $('#input-furnitureType').val(pDataObj.furnitureType);
  $('#input-priceRent').val(pDataObj.priceRent);

  $('#input-returnRate').val(pDataObj.returnRate);
  $('#input-legalDoc').val(pDataObj.legalDoc);
  $('#input-description').val(pDataObj.description);
  $('#input-widthY').val(pDataObj.widthY);
  $('#input-longX').val(pDataObj.longX);
  $('#input-streetHouse').val(pDataObj.streetHouse);
  $('#input-fsbo').val(pDataObj.fsbo);
  $('#input-viewNum').val(pDataObj.viewNum);
  $('#input-updateBy').val(pDataObj.updateBy);

  $('#input-shape').val(pDataObj.shape);
  $('#input-distance2facade').val(pDataObj.distance2facade);
  $('#input-adjacent_facade_num').val(pDataObj.adjacent_facade_num);
  $('#input-adjacentRoad').val(pDataObj.adjacentRoad);
  $('#input-alley_min_width').val(pDataObj.alley_min_width);
  $('#input-adjacent_alley_min_width').val(pDataObj.adjacent_alley_min_width);
  $('#input-factor').val(pDataObj.factor);
  $('#input-structure').val(pDataObj.structure);
  $('#input-dtsxd').val(pDataObj.dtsxd);
  $('#input-clcl').val(pDataObj.clcl);
  $('#input-ctxd_price').val(pDataObj.ctxd_price);

  $('#input-ctxd_value').val(pDataObj.ctxd_value);
  $('#input-photo').val(pDataObj.photo);
  $('#input-lat').val(pDataObj.lat);
  $('#input-lng').val(pDataObj.lng);
}
