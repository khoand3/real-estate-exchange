'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_EDIT = 0;
const gCOL_DELETE = 1;
const gCOL_REAL_ESTATE = 2;
const gCOL_ID = 3;
const gCOL_NAME = 4;
const gCOL_PROVINCE = 5;
const gCOL_DISTRICT = 6;
const gCOL_ACREAGE = 7;
const gCOL_CONSTRUCTION_AREA = 8;
const gCOL_NUM_BLOCK = 9;
const gCOL_NUM_FLOOR = 10;
const gCOL_NUM_APARTMENT = 11;
const gCOL_INVESTOR = 12;
const gCOL_CONSTRUCTION_CONTRACTOR = 13;
const gCOL_DESIGN_UNIT = 14;
const gCOL_PHOTO = 15;
const gDATA_COLS = [
  'edit',
  'delete',
  'real-estate',
  'id',
  'name',
  'district.province.name',
  'district.name',
  'acreage',
  'constructArea',
  'numBlock',
  'numFloors',
  'numApartment',
  'investor.name',
  'constructionContractor.name',
  'designUnit.name',
  'photo',
];
var gTable = $('#project-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
    { data: gDATA_COLS[gCOL_REAL_ESTATE] },
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_NAME] },
    { data: gDATA_COLS[gCOL_PROVINCE] },
    { data: gDATA_COLS[gCOL_DISTRICT] },
    { data: gDATA_COLS[gCOL_ACREAGE] },
    { data: gDATA_COLS[gCOL_CONSTRUCTION_AREA] },
    { data: gDATA_COLS[gCOL_NUM_BLOCK] },
    { data: gDATA_COLS[gCOL_NUM_FLOOR] },
    { data: gDATA_COLS[gCOL_NUM_APARTMENT] },
    { data: gDATA_COLS[gCOL_INVESTOR] },
    { data: gDATA_COLS[gCOL_CONSTRUCTION_CONTRACTOR] },
    { data: gDATA_COLS[gCOL_DESIGN_UNIT] },
    { data: gDATA_COLS[gCOL_PHOTO] },
  ],
  columnDefs: [
    {
      targets: gCOL_REAL_ESTATE,
      className: 'text-center',
      defaultContent: `<button title="Thông tin BĐS" class="btn btn-secondary btn-real-estate-detail" style="cursor: pointer">Thông tin BĐS</button>`,
    },
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_PHOTO,
      className: 'text-center',
      render: function (data) {
        return `<img src="${data}" width = "50%"/>`;
      },
    },
  ],
  autoWidth: false,
});
//bien toan cuc luu gia tri id
var gProjectId = 0;
var gProvinceId = 0;
var gDistrictId = 0;
var gInvestorId = -1;
var gConstructionContractorId = -1;
var gDesignUnitId = -1;
var gUtilitiesId = -1;
var gRegionLinkId = -1;
//bien toan cuc luu phan tu select
const gSelectProvinceElement = $('#select-province');
const gSelectDistrictElement = $('#select-district');
const gSelectInvestorElement = $('#select-investor');
const gSelectConstructionContractorElement = $(
  '#select-construction-contractor'
);
const gSelectDesignUnitElement = $('#select-design-unit');
const gSelectUtilitiesElement = $('#select-utilities');
const gSelectRegionLinkElement = $('#select-region-link');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gProjectId = 0;
    emptyDataInput();
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi chon tinh thanh
  $('#select-province').on('change', function () {
    onSelectProvinceChange(this);
  });
  //gan ham xu ly su kien khi chon quan huyen
  $('#select-district').on('change', function () {
    gDistrictId = $(this).val();
  });
  //gan ham xu ly su kien khi chon nha dau tu
  $('#select-investor').on('change', function () {
    gInvestorId = $(this).val();
  });
  //gan ham xu ly su kien khi chon nha thau xay dung
  $('#select-construction-contractor').on('change', function () {
    gConstructionContractorId = $(this).val();
  });
  //gan ham xu ly su kien khi chon don vi thiet ke
  $('#select-design-unit').on('change', function () {
    gDesignUnitId = $(this).val();
  });
  //gan ham xu ly su kien khi chon tien ich
  $('#select-utilities').on('change', function () {
    gUtilitiesId = $(this).val();
  });
  //gan ham xu ly su kien khi chon lien ket vung
  $('#select-region-link').on('change', function () {
    gRegionLinkId = $(this).val();
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#project-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#project-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/projects', loadDataToTable);
  callApiToGetAll('provinces', gSelectProvinceElement);
  callApiToGetAll('investors', gSelectInvestorElement);
  callApiToGetAll(
    'construction-contractors',
    gSelectConstructionContractorElement
  );
  callApiToGetAll('design-units', gSelectDesignUnitElement);
  callApiToGetAll('utilities', gSelectUtilitiesElement);
  callApiToGetAll('region-links', gSelectRegionLinkElement);
}
//ham xu ly su kien khi chon tinh thanh
function onSelectProvinceChange(pElement) {
  gProvinceId = $(pElement).val();
  $.get(
    'http://localhost:8080/provinces/' + gProvinceId + '/districts',
    loadDataToSelectDistrict
  );
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gProjectId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gProjectId = vCurrentRowData.id;
  gProvinceId = vCurrentRowData.district.province.id;
  gDistrictId = vCurrentRowData.district.id;
  gInvestorId = vCurrentRowData.investor.id;
  gConstructionContractorId = vCurrentRowData.constructionContractor.id;
  gDesignUnitId = vCurrentRowData.designUnit.id;
  gUtilitiesId = vCurrentRowData.utilities.id;
  gRegionLinkId = vCurrentRowData.regionLink.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gProjectId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
function getInputData() {
  var vDataObj = {
    name: '',
    address: '',
    slogan: '',
    description: '',
    latitude: '',
    longitude: '',
    acreage: '',
    constructArea: '',
    numBlock: '',
    numFloors: '',
    numApartment: '',
    apartmentArea: '',
    photo: '',
  };

  vDataObj.name = $('#input-name').val().trim();
  vDataObj.address = $('#input-address').val().trim();
  vDataObj.slogan = $('#txtarea-slogan').val().trim();
  vDataObj.description = $('#txtarea-description').val().trim();
  vDataObj.latitude = $('#input-latitude').val().trim();
  vDataObj.longitude = $('#input-longitude').val().trim();
  vDataObj.acreage = $('#input-acreage').val().trim();
  vDataObj.constructArea = $('#input-construct-area').val().trim();
  vDataObj.numBlock = $('#input-num-block').val().trim();
  vDataObj.numFloors = $('#input-num-floor').val().trim();
  vDataObj.numApartment = $('#input-num-apartment').val().trim();
  vDataObj.apartmentArea = $('#input-apartment-area').val().trim();
  vDataObj.photo = $('#txtarea-photo').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.name === '') {
      vResult = false;
      throw 'Vui lòng nhập tên dự án';
    }
    if (pDataObj.address === '') {
      vResult = false;
      throw 'Vui lòng nhập địa chỉ';
    }
    if (gSelectProvinceElement.val() === '0') {
      vResult = false;
      throw 'Vui lòng chọn tỉnh thành';
    }
    if (gSelectDistrictElement.val() === '0') {
      vResult = false;
      throw 'Vui lòng chọn quận huyện';
    }
    if (gSelectInvestorElement.val() === '-1') {
      vResult = false;
      throw 'Vui lòng chọn nhà đầu tư';
    }
    if (gSelectConstructionContractorElement.val() === '-1') {
      vResult = false;
      throw 'Vui lòng chọn nhà thầy xây dựng';
    }
    if (gSelectDesignUnitElement.val() === '-1') {
      vResult = false;
      throw 'Vui lòng chọn đơn vị thiết kế';
    }
    if (gSelectUtilitiesElement.val() === '-1') {
      vResult = false;
      throw 'Vui lòng chọn tiện ích';
    }
    if (gSelectRegionLinkElement.val() === '-1') {
      vResult = false;
      throw 'Vui lòng chọn liên kết vùng';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}

//ham goi api de them moi
function callApiToAdd(pDataObj) {
  console.log(gDesignUnitId);
  $.ajax({
    url: `http://localhost:8080/projects/${gProvinceId}/${gDistrictId}/${gInvestorId}/${gConstructionContractorId}/${gDesignUnitId}/${gUtilitiesId}/${gRegionLinkId}`,
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/projects', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: `http://localhost:8080/projects/${gProjectId}/${gProvinceId}/${gDistrictId}/${gInvestorId}/${gConstructionContractorId}/${gDesignUnitId}/${gUtilitiesId}/${gRegionLinkId}`,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/projects', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/projects/' + gProjectId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/projects', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#input-name').val('');
  $('#input-address').val('');
  $('#txtarea-slogan').val('');
  $('#txtarea-description').val('');
  $('#input-latitude').val('');
  $('#input-longitude').val('');
  $('#input-acreage').val('');
  $('#input-construct-area').val('');
  $('#input-num-block').val('');
  $('#input-num-floor').val('');
  $('#input-num-apartment').val('');
  $('#input-apartment-area').val('');
  $('#txtarea-photo').val('');
  gSelectProvinceElement.val('0');
  gSelectDistrictElement.val('0');
  gSelectDistrictElement.val('0');
  gSelectInvestorElement.val('-1');
  gSelectConstructionContractorElement.val('-1');
  gSelectDesignUnitElement.val('-1');
  gSelectUtilitiesElement.val('-1');
  gSelectRegionLinkElement.val('-1');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#input-name').val(pDataObj.name);
  $('#input-address').val(pDataObj.address);
  $('#txtarea-slogan').val(pDataObj.slogan);
  $('#txtarea-description').val(pDataObj.description);
  $('#input-latitude').val(pDataObj.latitude);
  $('#input-longitude').val(pDataObj.longitude);
  $('#input-acreage').val(pDataObj.acreage);
  $('#input-construct-area').val(pDataObj.constructArea);
  $('#input-num-block').val(pDataObj.numBlock);
  $('#input-num-floor').val(pDataObj.numFloors);
  $('#input-num-apartment').val(pDataObj.numApartment);
  $('#input-apartment-area').val(pDataObj.apartmentArea);
  $('#txtarea-photo').val(pDataObj.photo);
  gSelectProvinceElement.val(pDataObj.district.province.id);
  gSelectDistrictElement.append(
    `<option value=${pDataObj.district.id}>${pDataObj.district.name}</option>`
  );
  gSelectDistrictElement.val(pDataObj.district.id);
  gSelectInvestorElement.val(pDataObj.investor.id);
  gSelectConstructionContractorElement.val(pDataObj.constructionContractor.id);
  gSelectDesignUnitElement.val(pDataObj.designUnit.id);
  gSelectUtilitiesElement.val(pDataObj.utilities.id);
  gSelectRegionLinkElement.val(pDataObj.regionLink.id);
}
//ham goi api de lay tat ca thong tin
function callApiToGetAll(pEntityName, pElement) {
  $.ajax({
    url: 'http://localhost:8080/' + pEntityName,
    method: 'GET',
    success: function (res) {
      loadDataToSelectModal(pElement, res);
    },
    error: function (err) {
      console.log(err);
    },
  });
}
//ham load du lieu len select modal
function loadDataToSelectModal(pElement, pData) {
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = $(
      `<option value="${pData[bI].id}">${pData[bI].name}</option>`
    );
    pElement.append(bOption);
  }
}
//ham load du lieu len select district tren modal
function loadDataToSelectDistrict(pData) {
  gSelectDistrictElement.html('');
  gSelectDistrictElement.append(`<option value="0">Chọn quận huyện</option>`);
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = `<option value="${pData[bI].id}">${pData[bI].name}</option>`;
    gSelectDistrictElement.append(bOption);
  }
}
