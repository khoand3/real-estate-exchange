'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_EDIT = 0;
const gCOL_DELETE = 1;
const gCOL_ID = 2;
const gCOL_APART_CODE = 3;
const gCOL_PROJECT_NAME = 4;
const gCOL_PROVINCE = 5;
const gCOL_DISTRICT = 6;
const gCOL_ADDRESS = 7;
const gCOL_TYPE = 8;
const gCOL_REQUEST = 9;
const gCOL_PRICE = 10;
const gCOL_DATE_CREATE = 11;
const gCOL_ACREAGE = 12;
const gCOL_DESCRIPTION = 13;
const gCOL_PHOTO = 14;
const gDATA_COLS = [
  'edit',
  'delete',
  'id',
  'apartCode',
  'project.name',
  'district.province.name',
  'district.name',
  'address',
  'type',
  'request',
  'price',
  'dateCreate',
  'acreage',
  'description',
  'photo',
];
var gTable = $('#real-estate-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_APART_CODE] },
    { data: gDATA_COLS[gCOL_PROJECT_NAME] },
    { data: gDATA_COLS[gCOL_PROVINCE] },
    { data: gDATA_COLS[gCOL_DISTRICT] },
    { data: gDATA_COLS[gCOL_ADDRESS] },
    { data: gDATA_COLS[gCOL_TYPE] },
    { data: gDATA_COLS[gCOL_REQUEST] },
    { data: gDATA_COLS[gCOL_PRICE] },
    { data: gDATA_COLS[gCOL_DATE_CREATE] },
    { data: gDATA_COLS[gCOL_ACREAGE] },
    { data: gDATA_COLS[gCOL_DESCRIPTION] },
    { data: gDATA_COLS[gCOL_PHOTO] },
  ],
  columnDefs: [
    {
      targets: gCOL_TYPE,
      render: function (data) {
        if (data === 2) {
          return 'Căn hộ chung cư';
        }
      },
    },
    {
      targets: gCOL_REQUEST,
      render: function (data) {
        if (data === 0) {
          return 'Cần bán';
        }
      },
    },
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_PHOTO,
      className: 'text-center',
      render: function (data) {
        return `<img src="${data}" width = "50%"/>`;
      },
    },
  ],
  autoWidth: false,
});
//bien toan cuc luu gia tri id
var gRealEstateId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    window.location.href = './real-estate-add.html';
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#real-estate-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#real-estate-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/real-estates', loadDataToTable);
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gRealEstateId = vCurrentRowData.id;
  var vUrlSiteToOpen = './real-estate-add.html?id=' + gRealEstateId;
  window.location.href = vUrlSiteToOpen;
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gRealEstateId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/real-estates/' + gRealEstateId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/real-estates', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
