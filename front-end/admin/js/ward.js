'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_ID = 0;
const gCOL_NAME = 1;
const gCOL_PREFIX = 2;
const gCOL_PROVINCE = 3;
const gCOL_DISTRICT = 4;
const gCOL_EDIT = 5;
const gCOL_DELETE = 6;
const gDATA_COLS = [
  'id',
  'name',
  'prefix',
  'district.province.name',
  'district.name',
];
var gTable = $('#ward-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_NAME] },
    { data: gDATA_COLS[gCOL_PREFIX] },
    { data: gDATA_COLS[gCOL_PROVINCE] },
    { data: gDATA_COLS[gCOL_DISTRICT] },
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
  ],
  columnDefs: [
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
  autoWidth: false,
});
//bien toan cuc luu gia tri id
var gProvinceId = 0;
var gDistrictId = 0;
var gWardId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gWardId = 0;
    emptyDataInput();
    $('#select-province').prop('disabled', false);
    $('#select-district').prop('disabled', false);
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi chon tinh thanh
  $('#select-province').on('change', function () {
    onSelectProvinceChange(this);
  });
  //gan ham xu ly su kien khi chon quan huyen
  $('#select-district').on('change', function () {
    onSelectDistrictChange(this);
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#ward-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#ward-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/wards', loadDataToTable);
  $.get('http://localhost:8080/provinces', loadDataToSelectProvince);
}
//ham xu ly su kien khi chon tinh thanh
function onSelectProvinceChange(pElement) {
  gProvinceId = $(pElement).val();
  console.log(gProvinceId);
  $.get(
    'http://localhost:8080/provinces/' + gProvinceId + '/districts',
    loadDataToSelectDistrict
  );
}
//ham xu ly su kien khi chon quan huyen
function onSelectDistrictChange(pElement) {
  gDistrictId = $(pElement).val();
  console.log(gDistrictId);
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gWardId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  console.log(vCurrentRowData);
  gProvinceId = vCurrentRowData.district.province.id;
  gDistrictId = vCurrentRowData.district.id;
  gWardId = vCurrentRowData.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#select-province').prop('disabled', 'disabled');
  $('#select-district').prop('disabled', 'disabled');
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gWardId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
function getInputData() {
  var vDataObj = {
    name: '',
    prefix: '',
  };

  vDataObj.name = $('#input-name').val().trim();
  vDataObj.prefix = $('#select-prefix').val();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if ($('#select-province').val() === '0') {
      vResult = false;
      throw 'Vui lòng chọn tỉnh thành';
    }
    if ($('#select-district').val() === '0') {
      vResult = false;
      throw 'Vui lòng chọn quận huyện';
    }
    if (pDataObj.prefix === '0') {
      vResult = false;
      throw 'Vui lòng chọn tiếp đầu ngữ';
    }
    if (pDataObj.name === '') {
      vResult = false;
      throw 'Vui lòng nhập tên phường xã';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url:
      'http://localhost:8080/provinces/' +
      gProvinceId +
      '/districts/' +
      gDistrictId +
      '/wards',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/wards', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/wards/' + gWardId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/wards', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/wards/' + gWardId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/wards', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#select-province').val('0');
  $('#select-district').val('0');
  $('#select-prefix').val('0');
  $('#input-name').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#select-province').val(gProvinceId);
  $('#select-district').append(
    `<option value=${pDataObj.district.id}>${pDataObj.district.name}</option>`
  );
  $('#select-prefix').val(pDataObj.prefix);
  $('#input-name').val(pDataObj.name);
}
//ham load du lieu len select province tren modal
function loadDataToSelectProvince(pData) {
  var vSelectProvinceElement = $('#select-province');
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = `<option value="${pData[bI].id}">${pData[bI].name}</option>`;
    vSelectProvinceElement.append(bOption);
  }
}
//ham load du lieu len select district tren modal
function loadDataToSelectDistrict(pData) {
  var vSelectDistrictElement = $('#select-district');
  vSelectDistrictElement.html('');
  vSelectDistrictElement.append(`<option value="0">Chọn quận huyện</option>`);
  for (var bI = 0; bI < pData.length; bI++) {
    var bOption = `<option value="${pData[bI].id}">${pData[bI].name}</option>`;
    vSelectDistrictElement.append(bOption);
  }
}
