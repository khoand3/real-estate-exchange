'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_REAL_ESTATE = 0;
const gCOL_ID = 1;
const gCOL_NAME = 2;
const gCOL_ADDRESS = 3;
const gCOL_MOBILE = 4;
const gCOL_EMAIL = 5;
const gCOL_NOTE = 6;
const gCOL_DATE_CREATE = 7;
const gCOL_DATE_UPDATE = 8;
const gCOL_EDIT = 9;
const gCOL_DELETE = 10;
const gDATA_COLS = [
  'real-estate',
  'id',
  'contactName',
  'address',
  'mobile',
  'email',
  'note',
  'createDate',
  'updateDate',
];
var gTable = $('#customer-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_REAL_ESTATE] },
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_NAME] },
    { data: gDATA_COLS[gCOL_ADDRESS] },
    { data: gDATA_COLS[gCOL_MOBILE] },
    { data: gDATA_COLS[gCOL_EMAIL] },
    { data: gDATA_COLS[gCOL_NOTE] },
    { data: gDATA_COLS[gCOL_DATE_CREATE] },
    { data: gDATA_COLS[gCOL_DATE_UPDATE] },
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
  ],
  columnDefs: [
    {
      targets: gCOL_REAL_ESTATE,
      className: 'text-center',
      defaultContent: `<button title="Thông tin BĐS" class="btn btn-secondary btn-real-estate-detail" style="cursor: pointer">Thông tin BĐS</button>`,
    },
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
  autoWidth: false,
});
//bien toan cuc luu gia tri id
var gCustomerId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gCustomerId = 0;
    emptyDataInput();
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#customer-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#customer-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/customers', loadDataToTable);
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gCustomerId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gCustomerId = vCurrentRowData.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gCustomerId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
function getInputData() {
  var vDataObj = {
    contactName: '',
    address: '',
    mobile: '',
    email: '',
    note: '',
  };

  vDataObj.contactName = $('#input-name').val().trim();
  vDataObj.address = $('#input-address').val().trim();
  vDataObj.mobile = $('#input-mobile').val().trim();
  vDataObj.email = $('#input-email').val().trim();
  vDataObj.note = $('#txtarea-note').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.contactName === '') {
      vResult = false;
      throw 'Vui lòng nhập tên khách hàng';
    }
    if (pDataObj.address === '') {
      vResult = false;
      throw 'Vui lòng nhập địa chỉ';
    }
    if (pDataObj.mobile === '') {
      vResult = false;
      throw 'Vui lòng nhập số điện thoại';
    }
    if (isNaN(pDataObj.mobile)) {
      vResult = false;
      throw 'Số điện thoại phải là dãy số';
    }
    if (pDataObj.email !== '') {
      if (validateEmail(pDataObj.email) === false) {
        vResult = false;
      }
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham kiem tra email
function validateEmail(pEmail) {
  var vResultEmail = true;
  var validRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  try {
    if (!pEmail.match(validRegex)) {
      vResultEmail = false;
      throw 'Email không hợp lệ';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResultEmail;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/customers',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/customers', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/customers/' + gCustomerId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/customers', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/customers/' + gCustomerId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/customers', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#input-name').val('');
  $('#input-address').val('');
  $('#input-mobile').val('');
  $('#input-email').val('');
  $('#txtarea-note').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#input-name').val(pDataObj.contactName);
  $('#input-address').val(pDataObj.address);
  $('#input-mobile').val(pDataObj.mobile);
  $('#input-email').val(pDataObj.email);
  $('#txtarea-note').val(pDataObj.note);
}
