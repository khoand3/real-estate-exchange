'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//dinh nghia table
const gCOL_ID = 0;
const gCOL_ADDRESS = 1;
const gCOL_LATITUDE = 2;
const gCOL_LONGITUDE = 3;
const gCOL_EDIT = 4;
const gCOL_DELETE = 5;
const gDATA_COLS = ['id', 'address', 'latitude', 'longitude'];
var gTable = $('#address-map-table').DataTable({
  columns: [
    { data: gDATA_COLS[gCOL_ID] },
    { data: gDATA_COLS[gCOL_ADDRESS] },
    { data: gDATA_COLS[gCOL_LATITUDE] },
    { data: gDATA_COLS[gCOL_LONGITUDE] },
    { data: gDATA_COLS[gCOL_EDIT] },
    { data: gDATA_COLS[gCOL_DELETE] },
  ],
  columnDefs: [
    {
      targets: gCOL_EDIT,
      className: 'text-center',
      defaultContent: `<i title="Cập nhật" class="fas fa-edit text-info btn-edit" style="cursor: pointer"></i>`,
    },
    {
      targets: gCOL_DELETE,
      className: 'text-center',
      defaultContent: `<i title="Xóa" class="fas fa-trash text-danger btn-delete" style="cursor: pointer"></i>`,
    },
  ],
});
//bien toan cuc luu gia tri id
var gAddressMapId = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi click nut them
  $('#btn-add').on('click', function () {
    gAddressMapId = 0;
    emptyDataInput();
    $('#modal-add').modal('show');
  });
  //gan ham xu ly su kien khi click nut them tren modal
  $('#btn-add-modal').on('click', function () {
    onBtnAddModalClick();
  });
  //gan ham xu ly su kien khi click nut cap nhat
  $('#address-map-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa
  $('#address-map-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });
  //gan ham xu ly su kien khi click nut xoa tren modal
  $('#btn-delete-modal').on('click', function () {
    onBtnDeleteModalClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  $.get('http://localhost:8080/address-maps', loadDataToTable);
}
//ham xu ly su kien khi click nut them tren modal
function onBtnAddModalClick() {
  var vDataObj = getInputData();
  var vIsChecked = validateData(vDataObj);
  if (vIsChecked) {
    if (gAddressMapId === 0) {
      callApiToAdd(vDataObj);
    } else {
      callApiToUpdate(vDataObj);
    }
  }
}
//ham xu ly su kien khi click nut cap nhat
function onBtnEditClick(pElement) {
  emptyDataInput();
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gAddressMapId = vCurrentRowData.id;
  loadCurrentRowDataToInput(vCurrentRowData);
  $('#modal-add').modal('show');
}
//ham xu ly su kien khi click nut xoa
function onBtnDeleteClick(pElement) {
  var vCurrentRow = $(pElement).closest('tr');
  var vCurrentRowData = gTable.row(vCurrentRow).data();
  gAddressMapId = vCurrentRowData.id;
  $('#modal-delete').modal('show');
}
//ham xu ly su kien khi click nut xoa tren modal
function onBtnDeleteModalClick() {
  callApiToDelete();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu len table
function loadDataToTable(pData) {
  gTable.clear();
  gTable.rows.add(pData);
  gTable.draw();
}
function getInputData() {
  var vDataObj = {
    address: '',
    latitude: '',
    longitude: '',
  };

  vDataObj.address = $('#input-address').val().trim();
  vDataObj.latitude = $('#input-latitude').val().trim();
  vDataObj.longitude = $('#input-longitude').val().trim();

  return vDataObj;
}
//ham kiem tra du lieu
function validateData(pDataObj) {
  var vResult = true;
  try {
    if (pDataObj.address === '') {
      vResult = false;
      throw 'Vui lòng nhập tên địa điểm';
    }
    if (pDataObj.latitude === '') {
      vResult = false;
      throw 'Vui lòng nhập vĩ độ';
    }
    if (pDataObj.longitude === '') {
      vResult = false;
      throw 'Vui lòng nhập kinh độ';
    }
  } catch (error) {
    toastr.error(error);
  }
  return vResult;
}
//ham goi api de them moi
function callApiToAdd(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/address-maps',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Thêm mới thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/address-maps', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de cap nhat
function callApiToUpdate(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/address-maps/' + gAddressMapId,
    method: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(pDataObj),
    success: function (response) {
      console.log(response);
      toastr.success('Cập nhật thành công');
      $('#modal-add').modal('hide');
      $.get('http://localhost:8080/address-maps', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de xoa
function callApiToDelete(pDataObj) {
  $.ajax({
    url: 'http://localhost:8080/address-maps/' + gAddressMapId,
    method: 'DELETE',
    success: function (response) {
      console.log(response);
      toastr.success('Xóa thành công');
      $('#modal-delete').modal('hide');
      $.get('http://localhost:8080/address-maps', loadDataToTable);
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham empty input
function emptyDataInput() {
  $('#input-address').val('');
  $('#input-latitude').val('');
  $('#input-longitude').val('');
}
//ham load du lieu cua dong hien tai len input
function loadCurrentRowDataToInput(pDataObj) {
  $('#input-address').val(pDataObj.address);
  $('#input-latitude').val(pDataObj.latitude);
  $('#input-longitude').val(pDataObj.longitude);
}
