package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.ConstructionContractor;
import com.devcamp.realestate.repository.IConstructionContractorRepository;

@Service
public class ConstructionContractorService {
   @Autowired
   private IConstructionContractorRepository constructionContractorRepository;

   public List<ConstructionContractor> getAllConstructionContractor() {
      return constructionContractorRepository.findAll();
   }

   public ConstructionContractor getConstructionContractorById(int id) {
      Optional<ConstructionContractor> constructionContractorData = constructionContractorRepository.findById(id);
      if (constructionContractorData.isPresent()) {
         return constructionContractorData.get();
      } else {
         return null;
      }
   }

   public ConstructionContractor createConstructionContractor(ConstructionContractor pConstructionContractor) {
      ConstructionContractor newConstructionContractor = new ConstructionContractor();

      newConstructionContractor.setName(pConstructionContractor.getName());
      newConstructionContractor.setDescription(pConstructionContractor.getDescription());
      newConstructionContractor.setProjects(pConstructionContractor.getProjects());
      newConstructionContractor.setAddress(pConstructionContractor.getAddress());
      newConstructionContractor.setPhone(pConstructionContractor.getPhone());
      newConstructionContractor.setPhone2(pConstructionContractor.getPhone2());
      newConstructionContractor.setFax(pConstructionContractor.getFax());
      newConstructionContractor.setEmail(pConstructionContractor.getEmail());
      newConstructionContractor.setWebsite(pConstructionContractor.getWebsite());
      newConstructionContractor.setNote(pConstructionContractor.getNote());

      constructionContractorRepository.save(newConstructionContractor);

      return newConstructionContractor;
   }

   public ConstructionContractor updateConstructionContractor(int id, ConstructionContractor pConstructionContractor) {
      Optional<ConstructionContractor> ConstructionContractorData = constructionContractorRepository.findById(id);
      if (ConstructionContractorData.isPresent()) {
         ConstructionContractor updateConstructionContractor = ConstructionContractorData.get();

         updateConstructionContractor.setName(pConstructionContractor.getName());
         updateConstructionContractor.setDescription(pConstructionContractor.getDescription());
         updateConstructionContractor.setProjects(pConstructionContractor.getProjects());
         updateConstructionContractor.setAddress(pConstructionContractor.getAddress());
         updateConstructionContractor.setPhone(pConstructionContractor.getPhone());
         updateConstructionContractor.setPhone2(pConstructionContractor.getPhone2());
         updateConstructionContractor.setFax(pConstructionContractor.getFax());
         updateConstructionContractor.setEmail(pConstructionContractor.getEmail());
         updateConstructionContractor.setWebsite(pConstructionContractor.getWebsite());
         updateConstructionContractor.setNote(pConstructionContractor.getNote());

         constructionContractorRepository.save(updateConstructionContractor);

         return updateConstructionContractor;
      } else {
         return null;
      }
   }

   public Object deleteConstructionContractorById(int id) {
      Optional<ConstructionContractor> constructionContractorData = constructionContractorRepository.findById(id);
      if (constructionContractorData.isPresent()) {
         constructionContractorRepository.deleteById(id);
         return constructionContractorData;
      } else {
         return null;
      }
   }
}
