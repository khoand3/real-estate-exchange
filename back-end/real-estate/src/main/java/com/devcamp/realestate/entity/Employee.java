package com.devcamp.realestate.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "employees")
public class Employee {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "EmployeeID")
   private int id;

   @Column(name = "lastname", nullable = false)
   private String lastName;

   @Column(name = "firstname", nullable = false)
   private String firstName;

   @Column(name = "Title")
   private String title;

   @Column(name = "titleofcourtesy")
   private String titleOfCourtesy;

   @Column(name = "birthdate")
   @Temporal(TemporalType.TIMESTAMP)
   @JsonFormat(pattern = "dd-MM-yyyy")
   private Date birthDate;

   @Column(name = "hiredate")
   @Temporal(TemporalType.TIMESTAMP)
   @JsonFormat(pattern = "dd-MM-yyyy")
   private Date hireDate;

   @Column(name = "Address")
   private String address;

   @Column(name = "City")
   private String city;

   @Column(name = "Region")
   private String region;

   @Column(name = "postalcode")
   private String postalCode;

   @Column(name = "Country")
   private String country;

   @Column(name = "homephone")
   private String homePhone;

   @Column(name = "Extension")
   private String extension;

   @Column(name = "Photo")
   private String photo;

   @Column(name = "Notes")
   private String notes;

   @Column(name = "reportsto")
   private Integer reportsTo;

   @Column(name = "Username")
   private String username;

   @Column(name = "Password")
   private String password;

   @Column(name = "Email")
   private String email;

   @Column(name = "Activated", columnDefinition = "enum('Y','N')")
   private String activated;

   @Column(name = "Profile")
   private String profile;

   @Column(name = "userlevel")
   private Integer userLevel;

   public Employee() {
   }

   public Employee(int id, String lastName, String firstName, String title, String titleOfCourtesy, Date birthDate,
         Date hireDate, String address, String city, String region, String postalCode, String country, String homePhone,
         String extension, String photo, String notes, Integer reportsTo, String username, String password,
         String email, String activated, String profile, Integer userLevel) {
      this.id = id;
      this.lastName = lastName;
      this.firstName = firstName;
      this.title = title;
      this.titleOfCourtesy = titleOfCourtesy;
      this.birthDate = birthDate;
      this.hireDate = hireDate;
      this.address = address;
      this.city = city;
      this.region = region;
      this.postalCode = postalCode;
      this.country = country;
      this.homePhone = homePhone;
      this.extension = extension;
      this.photo = photo;
      this.notes = notes;
      this.reportsTo = reportsTo;
      this.username = username;
      this.password = password;
      this.email = email;
      this.activated = activated;
      this.profile = profile;
      this.userLevel = userLevel;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getLastName() {
      return lastName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public String getFirstName() {
      return firstName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getTitleOfCourtesy() {
      return titleOfCourtesy;
   }

   public void setTitleOfCourtesy(String titleOfCourtesy) {
      this.titleOfCourtesy = titleOfCourtesy;
   }

   public Date getBirthDate() {
      return birthDate;
   }

   public void setBirthDate(Date birthDate) {
      this.birthDate = birthDate;
   }

   public Date getHireDate() {
      return hireDate;
   }

   public void setHireDate(Date hireDate) {
      this.hireDate = hireDate;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getCity() {
      return city;
   }

   public void setCity(String city) {
      this.city = city;
   }

   public String getRegion() {
      return region;
   }

   public void setRegion(String region) {
      this.region = region;
   }

   public String getPostalCode() {
      return postalCode;
   }

   public void setPostalCode(String postalCode) {
      this.postalCode = postalCode;
   }

   public String getCountry() {
      return country;
   }

   public void setCountry(String country) {
      this.country = country;
   }

   public String getHomePhone() {
      return homePhone;
   }

   public void setHomePhone(String homePhone) {
      this.homePhone = homePhone;
   }

   public String getExtension() {
      return extension;
   }

   public void setExtension(String extension) {
      this.extension = extension;
   }

   public String getPhoto() {
      return photo;
   }

   public void setPhoto(String photo) {
      this.photo = photo;
   }

   public String getNotes() {
      return notes;
   }

   public void setNotes(String notes) {
      this.notes = notes;
   }

   public Integer getReportsTo() {
      return reportsTo;
   }

   public void setReportsTo(Integer reportsTo) {
      this.reportsTo = reportsTo;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getActivated() {
      return activated;
   }

   public void setActivated(String activated) {
      this.activated = activated;
   }

   public String getProfile() {
      return profile;
   }

   public void setProfile(String profile) {
      this.profile = profile;
   }

   public Integer getUserLevel() {
      return userLevel;
   }

   public void setUserLevel(Integer userLevel) {
      this.userLevel = userLevel;
   }

}
