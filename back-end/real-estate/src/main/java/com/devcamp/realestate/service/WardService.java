package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Ward;
import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.repository.IWardRepository;
import com.devcamp.realestate.repository.IDistrictRepository;
import com.devcamp.realestate.repository.IProvinceRepository;

@Service
public class WardService {
   @Autowired
   private IProvinceRepository provinceRepository;

   @Autowired
   private IDistrictRepository districtRepository;

   @Autowired
   private IWardRepository wardRepository;

   public List<Ward> getAllWard() {
      return wardRepository.findAll();
   }

   public List<Ward> getAllWards() {
      return wardRepository.getAllWards();
   }

   public Ward getWardById(int id) {
      Optional<Ward> wardData = wardRepository.findById(id);
      if (wardData.isPresent()) {
         return wardData.get();
      } else {
         return null;
      }
   }

   public Ward createWard(int provincesId, int districtId, Ward pWard) {
      Optional<Province> provinceData = provinceRepository.findById(provincesId);
      Optional<District> districtData = districtRepository.findById(districtId);
      if (provinceData.isPresent() && districtData.isPresent()) {
         Ward newWard = new Ward();

         newWard.setPrefix(pWard.getPrefix());
         newWard.setName(pWard.getName());
         newWard.setDistrict(districtData.get());
         newWard.setProvince(provinceData.get());

         wardRepository.save(newWard);

         return newWard;
      } else {
         return null;
      }
   }

   public Ward updateWard(int id, Ward pWard) {
      Optional<Ward> wardData = wardRepository.findById(id);
      if (wardData.isPresent()) {
         Ward updateWard = wardData.get();

         updateWard.setPrefix(pWard.getPrefix());
         updateWard.setName(pWard.getName());

         wardRepository.save(updateWard);

         return updateWard;
      } else {
         return null;
      }
   }

   public Object deleteWardById(int id) {
      Optional<Ward> wardData = wardRepository.findById(id);
      if (wardData.isPresent()) {
         wardRepository.deleteById(id);
         return wardData;
      } else {
         return null;
      }
   }
}
