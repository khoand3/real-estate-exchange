package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.ConstructionContractor;
import com.devcamp.realestate.entity.DesignUnit;
import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.entity.Investor;
import com.devcamp.realestate.entity.Project;
import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.entity.RegionLink;
import com.devcamp.realestate.entity.Utilities;
import com.devcamp.realestate.repository.IConstructionContractorRepository;
import com.devcamp.realestate.repository.IDesignUnitRepository;
import com.devcamp.realestate.repository.IDistrictRepository;
import com.devcamp.realestate.repository.IInvestorRepository;
import com.devcamp.realestate.repository.IProjectRepository;
import com.devcamp.realestate.repository.IProvinceRepository;
import com.devcamp.realestate.repository.IRegionLinkRepository;
import com.devcamp.realestate.repository.IUtilitiesRepository;

@Service
public class ProjectService {
   @Autowired
   private IProjectRepository projectRepository;

   @Autowired
   private IProvinceRepository provinceRepository;
   @Autowired
   private IDistrictRepository districtRepository;
   @Autowired
   private IInvestorRepository investorRepository;
   @Autowired
   private IConstructionContractorRepository constructionContractorRepository;
   @Autowired
   private IDesignUnitRepository designUnitRepository;
   @Autowired
   private IUtilitiesRepository utilitiesRepository;
   @Autowired
   private IRegionLinkRepository regionLinkRepository;

   public List<Project> getAllProject() {
      return projectRepository.findAll();
   }

   public Project getProjectById(int id) {
      Optional<Project> projectData = projectRepository.findById(id);
      if (projectData.isPresent()) {
         return projectData.get();
      } else {
         return null;
      }
   }

   public Project createProject(Project pProject, int provinceId, int districtId, int investorId,
         int constructionContractorId, int designUnitId, int utilitiesId, int regionLinkId) {
      Optional<Province> provinceData = provinceRepository.findById(provinceId);
      Optional<District> districtData = districtRepository.findById(districtId);
      Optional<Investor> investorData = investorRepository.findById(investorId);
      Optional<ConstructionContractor> constructionContractorData = constructionContractorRepository
            .findById(constructionContractorId);
      Optional<DesignUnit> designUnitData = designUnitRepository.findById(designUnitId);
      Optional<Utilities> utilitiesData = utilitiesRepository.findById(utilitiesId);
      Optional<RegionLink> regionLinkData = regionLinkRepository.findById(regionLinkId);

      Project newProject = new Project();

      newProject.setName(pProject.getName());
      newProject.setAddress(pProject.getAddress());
      newProject.setSlogan(pProject.getSlogan());
      newProject.setDescription(pProject.getDescription());
      newProject.setAcreage(pProject.getAcreage());
      newProject.setConstructArea(pProject.getConstructArea());
      newProject.setNumBlock(pProject.getNumBlock());
      newProject.setNumFloors(pProject.getNumFloors());
      newProject.setNumApartment(pProject.getNumApartment());
      newProject.setApartmentArea(pProject.getApartmentArea());
      newProject.setPhoto(pProject.getPhoto());
      newProject.setLatitude(pProject.getLatitude());
      newProject.setLongitude(pProject.getLongitude());

      newProject.setProvince(provinceData.get());
      newProject.setDistrict(districtData.get());
      newProject.setInvestor(investorData.get());
      newProject.setConstructionContractor(constructionContractorData.get());
      newProject.setDesignUnit(designUnitData.get());
      newProject.setUtilities(utilitiesData.get());
      newProject.setRegionLink(regionLinkData.get());

      projectRepository.save(newProject);

      return newProject;
   }

   public Project updateProject(int id, Project pProject, int provinceId, int districtId, int investorId,
         int constructionContractorId, int designUnitId, int utilitiesId, int regionLinkId) {
      Optional<Project> projectData = projectRepository.findById(id);
      Optional<Province> provinceData = provinceRepository.findById(provinceId);
      Optional<District> districtData = districtRepository.findById(districtId);
      Optional<Investor> investorData = investorRepository.findById(investorId);
      Optional<ConstructionContractor> constructionContractorData = constructionContractorRepository
            .findById(constructionContractorId);
      Optional<DesignUnit> designUnitData = designUnitRepository.findById(designUnitId);
      Optional<Utilities> utilitiesData = utilitiesRepository.findById(utilitiesId);
      Optional<RegionLink> regionLinkData = regionLinkRepository.findById(regionLinkId);
      if (projectData.isPresent()) {
         Project updateProject = projectData.get();

         updateProject.setName(pProject.getName());
         updateProject.setAddress(pProject.getAddress());
         updateProject.setSlogan(pProject.getSlogan());
         updateProject.setDescription(pProject.getDescription());
         updateProject.setAcreage(pProject.getAcreage());
         updateProject.setConstructArea(pProject.getConstructArea());
         updateProject.setNumBlock(pProject.getNumBlock());
         updateProject.setNumFloors(pProject.getNumFloors());
         updateProject.setNumApartment(pProject.getNumApartment());
         updateProject.setApartmentArea(pProject.getApartmentArea());
         updateProject.setPhoto(pProject.getPhoto());
         updateProject.setLatitude(pProject.getLatitude());
         updateProject.setLongitude(pProject.getLongitude());

         updateProject.setProvince(provinceData.get());
         updateProject.setDistrict(districtData.get());
         updateProject.setInvestor(investorData.get());
         updateProject.setConstructionContractor(constructionContractorData.get());
         updateProject.setDesignUnit(designUnitData.get());
         updateProject.setUtilities(utilitiesData.get());
         updateProject.setRegionLink(regionLinkData.get());

         projectRepository.save(updateProject);

         return updateProject;
      } else {
         return null;
      }
   }

   public Object deleteProjectById(int id) {
      Optional<Project> projectData = projectRepository.findById(id);
      if (projectData.isPresent()) {
         projectRepository.deleteById(id);
         return projectData;
      } else {
         return null;
      }
   }
}
