package com.devcamp.realestate.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Customer;
import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.entity.Project;
import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.entity.RealEstate;
import com.devcamp.realestate.repository.ICustomerRepository;
import com.devcamp.realestate.repository.IDistrictRepository;
import com.devcamp.realestate.repository.IProjectRepository;
import com.devcamp.realestate.repository.IProvinceRepository;
import com.devcamp.realestate.repository.IRealEstateRepository;

@Service
public class RealEstateService {
   @Autowired
   private IRealEstateRepository realEstateRepository;
   @Autowired
   private IProvinceRepository provinceRepository;
   @Autowired
   private IDistrictRepository districtRepository;
   @Autowired
   private ICustomerRepository customerRepository;
   @Autowired
   private IProjectRepository projectRepository;

   public List<RealEstate> getAllRealEstate() {
      return realEstateRepository.findAll();
   }

   public RealEstate getRealEstateById(int id) {
      Optional<RealEstate> realEstateData = realEstateRepository.findById(id);
      if (realEstateData.isPresent()) {
         return realEstateData.get();
      } else {
         return null;
      }
   }

   public RealEstate createRealEstate(RealEstate pRealEstate, int provinceId, int districtId, int projectId,
         Integer customerId) {
      Optional<Province> provinceData = provinceRepository.findById(provinceId);
      Optional<District> districtData = districtRepository.findById(districtId);
      Optional<Project> projectData = projectRepository.findById(projectId);
      Optional<Customer> customerData = customerRepository.findById(customerId);

      RealEstate newRealEstate = new RealEstate();

      newRealEstate.setTitle(pRealEstate.getTitle());
      newRealEstate.setType(pRealEstate.getType());
      newRealEstate.setRequest(pRealEstate.getRequest());
      newRealEstate.setAddress(pRealEstate.getAddress());
      newRealEstate.setPrice(pRealEstate.getPrice());
      newRealEstate.setPriceMin(pRealEstate.getPriceMin());
      newRealEstate.setPriceTime(pRealEstate.getPriceTime());
      newRealEstate.setDateCreate(new Date());
      newRealEstate.setAcreage(pRealEstate.getAcreage());
      newRealEstate.setDirection(pRealEstate.getDirection());
      newRealEstate.setTotalFloors(pRealEstate.getTotalFloors());
      newRealEstate.setNumberFloors(pRealEstate.getNumberFloors());
      newRealEstate.setBath(pRealEstate.getBath());
      newRealEstate.setApartCode(pRealEstate.getApartCode());
      newRealEstate.setWallArea(pRealEstate.getWallArea());
      newRealEstate.setBedroom(pRealEstate.getBedroom());
      newRealEstate.setBalcony(pRealEstate.getBalcony());
      newRealEstate.setLandscapeView(pRealEstate.getLandscapeView());
      newRealEstate.setApartLoca(pRealEstate.getApartLoca());
      newRealEstate.setApartType(pRealEstate.getApartType());
      newRealEstate.setFurnitureType(pRealEstate.getFurnitureType());
      newRealEstate.setPriceRent(pRealEstate.getPriceRent());
      newRealEstate.setReturnRate(pRealEstate.getReturnRate());
      newRealEstate.setLegalDoc(pRealEstate.getLegalDoc());
      newRealEstate.setDescription(pRealEstate.getDescription());
      newRealEstate.setWidthY(pRealEstate.getWidthY());
      newRealEstate.setLongX(pRealEstate.getLongX());
      newRealEstate.setStreetHouse(pRealEstate.getStreetHouse());
      newRealEstate.setFSBO(pRealEstate.getFSBO());
      newRealEstate.setViewNum(pRealEstate.getViewNum());
      newRealEstate.setCreateBy(pRealEstate.getCreateBy());
      newRealEstate.setUpdateBy(pRealEstate.getUpdateBy());
      newRealEstate.setShape(pRealEstate.getShape());
      newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
      newRealEstate.setAdjacentFacadeNum(pRealEstate.getAdjacentFacadeNum());
      newRealEstate.setAdjacentRoad(pRealEstate.getAdjacentRoad());
      newRealEstate.setAlleyMinWidth(pRealEstate.getAlleyMinWidth());
      newRealEstate.setFactor(pRealEstate.getFactor());
      newRealEstate.setStructure(pRealEstate.getStructure());
      newRealEstate.setDTSXD(pRealEstate.getDTSXD());
      newRealEstate.setCLCL(pRealEstate.getCLCL());
      newRealEstate.setCTXDPrice(pRealEstate.getCTXDPrice());
      newRealEstate.setCTXDValue(pRealEstate.getCTXDValue());
      newRealEstate.setPhoto(pRealEstate.getPhoto());
      newRealEstate.setLatitude(pRealEstate.getLatitude());
      newRealEstate.setLongitude(pRealEstate.getLongitude());

      newRealEstate.setProvince(provinceData.get());
      newRealEstate.setDistrict(districtData.get());
      newRealEstate.setProject(projectData.get());
      newRealEstate.setCustomer(customerData.get());

      realEstateRepository.save(newRealEstate);

      return newRealEstate;
   }

   public RealEstate createRealEstateNoCustomer(RealEstate pRealEstate, int provinceId, int districtId, int projectId) {
      Optional<Province> provinceData = provinceRepository.findById(provinceId);
      Optional<District> districtData = districtRepository.findById(districtId);
      Optional<Project> projectData = projectRepository.findById(projectId);

      RealEstate newRealEstate = new RealEstate();

      newRealEstate.setTitle(pRealEstate.getTitle());
      newRealEstate.setType(pRealEstate.getType());
      newRealEstate.setRequest(pRealEstate.getRequest());
      newRealEstate.setAddress(pRealEstate.getAddress());
      newRealEstate.setPrice(pRealEstate.getPrice());
      newRealEstate.setPriceMin(pRealEstate.getPriceMin());
      newRealEstate.setPriceTime(pRealEstate.getPriceTime());
      newRealEstate.setDateCreate(new Date());
      newRealEstate.setAcreage(pRealEstate.getAcreage());
      newRealEstate.setDirection(pRealEstate.getDirection());
      newRealEstate.setTotalFloors(pRealEstate.getTotalFloors());
      newRealEstate.setNumberFloors(pRealEstate.getNumberFloors());
      newRealEstate.setBath(pRealEstate.getBath());
      newRealEstate.setApartCode(pRealEstate.getApartCode());
      newRealEstate.setWallArea(pRealEstate.getWallArea());
      newRealEstate.setBedroom(pRealEstate.getBedroom());
      newRealEstate.setBalcony(pRealEstate.getBalcony());
      newRealEstate.setLandscapeView(pRealEstate.getLandscapeView());
      newRealEstate.setApartLoca(pRealEstate.getApartLoca());
      newRealEstate.setApartType(pRealEstate.getApartType());
      newRealEstate.setFurnitureType(pRealEstate.getFurnitureType());
      newRealEstate.setPriceRent(pRealEstate.getPriceRent());
      newRealEstate.setReturnRate(pRealEstate.getReturnRate());
      newRealEstate.setLegalDoc(pRealEstate.getLegalDoc());
      newRealEstate.setDescription(pRealEstate.getDescription());
      newRealEstate.setWidthY(pRealEstate.getWidthY());
      newRealEstate.setLongX(pRealEstate.getLongX());
      newRealEstate.setStreetHouse(pRealEstate.getStreetHouse());
      newRealEstate.setFSBO(pRealEstate.getFSBO());
      newRealEstate.setViewNum(pRealEstate.getViewNum());
      newRealEstate.setCreateBy(pRealEstate.getCreateBy());
      newRealEstate.setUpdateBy(pRealEstate.getUpdateBy());
      newRealEstate.setShape(pRealEstate.getShape());
      newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
      newRealEstate.setAdjacentFacadeNum(pRealEstate.getAdjacentFacadeNum());
      newRealEstate.setAdjacentRoad(pRealEstate.getAdjacentRoad());
      newRealEstate.setAlleyMinWidth(pRealEstate.getAlleyMinWidth());
      newRealEstate.setFactor(pRealEstate.getFactor());
      newRealEstate.setStructure(pRealEstate.getStructure());
      newRealEstate.setDTSXD(pRealEstate.getDTSXD());
      newRealEstate.setCLCL(pRealEstate.getCLCL());
      newRealEstate.setCTXDPrice(pRealEstate.getCTXDPrice());
      newRealEstate.setCTXDValue(pRealEstate.getCTXDValue());
      newRealEstate.setPhoto(pRealEstate.getPhoto());
      newRealEstate.setLatitude(pRealEstate.getLatitude());
      newRealEstate.setLongitude(pRealEstate.getLongitude());

      newRealEstate.setProvince(provinceData.get());
      newRealEstate.setDistrict(districtData.get());
      newRealEstate.setProject(projectData.get());

      realEstateRepository.save(newRealEstate);

      return newRealEstate;
   }

   public RealEstate updateRealEstate(int id, RealEstate pRealEstate, int provinceId, int districtId, int projectId,
         Integer customerId) {
      Optional<RealEstate> realEstateData = realEstateRepository.findById(id);
      Optional<Province> provinceData = provinceRepository.findById(provinceId);
      Optional<District> districtData = districtRepository.findById(districtId);
      Optional<Project> projectData = projectRepository.findById(projectId);
      Optional<Customer> customerData = customerRepository.findById(customerId);
      if (realEstateData.isPresent()) {
         RealEstate updateRealEstate = realEstateData.get();

         updateRealEstate.setTitle(pRealEstate.getTitle());
         updateRealEstate.setType(pRealEstate.getType());
         updateRealEstate.setRequest(pRealEstate.getRequest());

         updateRealEstate.setAddress(pRealEstate.getAddress());

         updateRealEstate.setPrice(pRealEstate.getPrice());
         updateRealEstate.setPriceMin(pRealEstate.getPriceMin());
         updateRealEstate.setPriceTime(pRealEstate.getPriceTime());
         updateRealEstate.setAcreage(pRealEstate.getAcreage());
         updateRealEstate.setDirection(pRealEstate.getDirection());
         updateRealEstate.setTotalFloors(pRealEstate.getTotalFloors());
         updateRealEstate.setNumberFloors(pRealEstate.getNumberFloors());
         updateRealEstate.setBath(pRealEstate.getBath());
         updateRealEstate.setApartCode(pRealEstate.getApartCode());
         updateRealEstate.setWallArea(pRealEstate.getWallArea());
         updateRealEstate.setBedroom(pRealEstate.getBedroom());
         updateRealEstate.setBalcony(pRealEstate.getBalcony());
         updateRealEstate.setLandscapeView(pRealEstate.getLandscapeView());
         updateRealEstate.setApartLoca(pRealEstate.getApartLoca());
         updateRealEstate.setApartType(pRealEstate.getApartType());
         updateRealEstate.setFurnitureType(pRealEstate.getFurnitureType());
         updateRealEstate.setPriceRent(pRealEstate.getPriceRent());
         updateRealEstate.setReturnRate(pRealEstate.getReturnRate());
         updateRealEstate.setLegalDoc(pRealEstate.getLegalDoc());
         updateRealEstate.setDescription(pRealEstate.getDescription());
         updateRealEstate.setWidthY(pRealEstate.getWidthY());
         updateRealEstate.setLongX(pRealEstate.getLongX());
         updateRealEstate.setStreetHouse(pRealEstate.getStreetHouse());
         updateRealEstate.setFSBO(pRealEstate.getFSBO());
         updateRealEstate.setViewNum(pRealEstate.getViewNum());
         updateRealEstate.setCreateBy(pRealEstate.getCreateBy());
         updateRealEstate.setUpdateBy(pRealEstate.getUpdateBy());
         updateRealEstate.setShape(pRealEstate.getShape());
         updateRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
         updateRealEstate.setAdjacentFacadeNum(pRealEstate.getAdjacentFacadeNum());
         updateRealEstate.setAdjacentRoad(pRealEstate.getAdjacentRoad());
         updateRealEstate.setAlleyMinWidth(pRealEstate.getAlleyMinWidth());
         updateRealEstate.setFactor(pRealEstate.getFactor());
         updateRealEstate.setStructure(pRealEstate.getStructure());
         updateRealEstate.setDTSXD(pRealEstate.getDTSXD());
         updateRealEstate.setCLCL(pRealEstate.getCLCL());
         updateRealEstate.setCTXDPrice(pRealEstate.getCTXDPrice());
         updateRealEstate.setCTXDValue(pRealEstate.getCTXDValue());
         updateRealEstate.setPhoto(pRealEstate.getPhoto());
         updateRealEstate.setLatitude(pRealEstate.getLatitude());
         updateRealEstate.setLongitude(pRealEstate.getLongitude());

         updateRealEstate.setProvince(provinceData.get());
         updateRealEstate.setDistrict(districtData.get());
         updateRealEstate.setProject(projectData.get());
         updateRealEstate.setCustomer(customerData.get());

         realEstateRepository.save(updateRealEstate);

         return updateRealEstate;
      } else {
         return null;
      }
   }

   public RealEstate updateRealEstateNoCustomer(int id, RealEstate pRealEstate, int provinceId, int districtId,
         int projectId) {
      Optional<RealEstate> realEstateData = realEstateRepository.findById(id);
      Optional<Province> provinceData = provinceRepository.findById(provinceId);
      Optional<District> districtData = districtRepository.findById(districtId);
      Optional<Project> projectData = projectRepository.findById(projectId);
      if (realEstateData.isPresent()) {
         RealEstate updateRealEstate = realEstateData.get();

         updateRealEstate.setTitle(pRealEstate.getTitle());
         updateRealEstate.setType(pRealEstate.getType());
         updateRealEstate.setRequest(pRealEstate.getRequest());

         updateRealEstate.setAddress(pRealEstate.getAddress());

         updateRealEstate.setPrice(pRealEstate.getPrice());
         updateRealEstate.setPriceMin(pRealEstate.getPriceMin());
         updateRealEstate.setPriceTime(pRealEstate.getPriceTime());
         updateRealEstate.setAcreage(pRealEstate.getAcreage());
         updateRealEstate.setDirection(pRealEstate.getDirection());
         updateRealEstate.setTotalFloors(pRealEstate.getTotalFloors());
         updateRealEstate.setNumberFloors(pRealEstate.getNumberFloors());
         updateRealEstate.setBath(pRealEstate.getBath());
         updateRealEstate.setApartCode(pRealEstate.getApartCode());
         updateRealEstate.setWallArea(pRealEstate.getWallArea());
         updateRealEstate.setBedroom(pRealEstate.getBedroom());
         updateRealEstate.setBalcony(pRealEstate.getBalcony());
         updateRealEstate.setLandscapeView(pRealEstate.getLandscapeView());
         updateRealEstate.setApartLoca(pRealEstate.getApartLoca());
         updateRealEstate.setApartType(pRealEstate.getApartType());
         updateRealEstate.setFurnitureType(pRealEstate.getFurnitureType());
         updateRealEstate.setPriceRent(pRealEstate.getPriceRent());
         updateRealEstate.setReturnRate(pRealEstate.getReturnRate());
         updateRealEstate.setLegalDoc(pRealEstate.getLegalDoc());
         updateRealEstate.setDescription(pRealEstate.getDescription());
         updateRealEstate.setWidthY(pRealEstate.getWidthY());
         updateRealEstate.setLongX(pRealEstate.getLongX());
         updateRealEstate.setStreetHouse(pRealEstate.getStreetHouse());
         updateRealEstate.setFSBO(pRealEstate.getFSBO());
         updateRealEstate.setViewNum(pRealEstate.getViewNum());
         updateRealEstate.setCreateBy(pRealEstate.getCreateBy());
         updateRealEstate.setUpdateBy(pRealEstate.getUpdateBy());
         updateRealEstate.setShape(pRealEstate.getShape());
         updateRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
         updateRealEstate.setAdjacentFacadeNum(pRealEstate.getAdjacentFacadeNum());
         updateRealEstate.setAdjacentRoad(pRealEstate.getAdjacentRoad());
         updateRealEstate.setAlleyMinWidth(pRealEstate.getAlleyMinWidth());
         updateRealEstate.setFactor(pRealEstate.getFactor());
         updateRealEstate.setStructure(pRealEstate.getStructure());
         updateRealEstate.setDTSXD(pRealEstate.getDTSXD());
         updateRealEstate.setCLCL(pRealEstate.getCLCL());
         updateRealEstate.setCTXDPrice(pRealEstate.getCTXDPrice());
         updateRealEstate.setCTXDValue(pRealEstate.getCTXDValue());
         updateRealEstate.setPhoto(pRealEstate.getPhoto());
         updateRealEstate.setLatitude(pRealEstate.getLatitude());
         updateRealEstate.setLongitude(pRealEstate.getLongitude());

         updateRealEstate.setProvince(provinceData.get());
         updateRealEstate.setDistrict(districtData.get());
         updateRealEstate.setProject(projectData.get());

         realEstateRepository.save(updateRealEstate);

         return updateRealEstate;
      } else {
         return null;
      }
   }

   public Object deleteRealEstateById(int id) {
      Optional<RealEstate> realEstateData = realEstateRepository.findById(id);
      if (realEstateData.isPresent()) {
         realEstateRepository.deleteById(id);
         return realEstateData;
      } else {
         return null;
      }
   }
}
