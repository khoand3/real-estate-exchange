package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.RealEstate;
import com.devcamp.realestate.service.RealEstateService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class RealEstateController {
   @Autowired
   private RealEstateService realEstateService;

   @GetMapping("/real-estates")
   public ResponseEntity<List<RealEstate>> getAllRealEstate() {
      try {
         return new ResponseEntity<>(realEstateService.getAllRealEstate(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/real-estates/{id}")
   public ResponseEntity<RealEstate> getRealEstateById(@PathVariable("id") int id) {
      try {
         RealEstate realEstate = realEstateService.getRealEstateById(id);
         if (realEstate != null) {
            return new ResponseEntity<>(realEstate, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping(value = { "/real-estates/{provinceId}/{districtId}/{projectId}",
         "/real-estates/{provinceId}/{districtId}/{projectId}/{customerId}"
   })
   public ResponseEntity<RealEstate> createRealEstate(@PathVariable("provinceId") int provinceId,
         @PathVariable("districtId") int districtId, @PathVariable("projectId") int projectId,
         @PathVariable(required = false) Integer customerId,
         @RequestBody RealEstate pRealEstate) {
      try {
         if (customerId != null) {
            return new ResponseEntity<>(
                  realEstateService.createRealEstate(pRealEstate, provinceId, districtId, projectId, customerId),
                  HttpStatus.CREATED);
         } else {
            return new ResponseEntity<>(
                  realEstateService.createRealEstateNoCustomer(pRealEstate, provinceId, districtId, projectId),
                  HttpStatus.CREATED);
         }

      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping(value = { "/real-estates/{id}/{provinceId}/{districtId}/{projectId}",
         "/real-estates/{id}/{provinceId}/{districtId}/{projectId}/{customerId}"
   })
   public ResponseEntity<RealEstate> updateRealEstateById(@PathVariable("id") int id,
         @PathVariable("provinceId") int provinceId,
         @PathVariable("districtId") int districtId, @PathVariable("projectId") int projectId,
         @PathVariable(required = false) Integer customerId,
         @RequestBody RealEstate pRealEstate) {
      try {
         if (customerId != null) {
            RealEstate updateRealEstate = realEstateService.updateRealEstate(id, pRealEstate, provinceId, districtId,
                  projectId, customerId);
            if (updateRealEstate != null) {
               return new ResponseEntity<>(updateRealEstate, HttpStatus.OK);
            } else {
               return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
         } else {
            RealEstate updateRealEstate = realEstateService.updateRealEstateNoCustomer(id, pRealEstate, provinceId,
                  districtId, projectId);
            if (updateRealEstate != null) {
               return new ResponseEntity<>(updateRealEstate, HttpStatus.OK);
            } else {
               return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/real-estates/{id}")
   public ResponseEntity<Object> deleteRealEstateById(@PathVariable("id") int id) {
      try {
         Object deleteRealEstate = realEstateService.deleteRealEstateById(id);
         if (deleteRealEstate != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
