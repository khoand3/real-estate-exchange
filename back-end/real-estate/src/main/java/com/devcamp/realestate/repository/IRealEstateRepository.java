package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.RealEstate;

public interface IRealEstateRepository extends JpaRepository<RealEstate, Integer> {

}
