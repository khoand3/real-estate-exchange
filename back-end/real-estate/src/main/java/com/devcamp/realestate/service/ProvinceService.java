package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.repository.IProvinceRepository;

@Service
public class ProvinceService {
   @Autowired
   private IProvinceRepository provinceRepository;

   public List<Province> getAllProvince() {
      return provinceRepository.findAll();
   }

   public Province getProvinceById(int id) {
      Optional<Province> provinceData = provinceRepository.findById(id);
      if (provinceData.isPresent()) {
         return provinceData.get();
      } else {
         return null;
      }
   }

   public Province createProvince(Province pProvince) {
      Province newProvince = new Province();

      newProvince.setCode(pProvince.getCode());
      newProvince.setName(pProvince.getName());

      provinceRepository.save(newProvince);

      return newProvince;
   }

   public Province updateProvince(int id, Province pProvince) {
      Optional<Province> provinceData = provinceRepository.findById(id);
      if (provinceData.isPresent()) {
         Province updateProvince = provinceData.get();

         updateProvince.setCode(pProvince.getCode());
         updateProvince.setName(pProvince.getName());

         provinceRepository.save(updateProvince);

         return updateProvince;
      } else {
         return null;
      }
   }

   public Object deleteProvinceById(int id) {
      Optional<Province> provinceData = provinceRepository.findById(id);
      if (provinceData.isPresent()) {
         provinceRepository.deleteById(id);
         return provinceData;
      } else {
         return null;
      }
   }
}
