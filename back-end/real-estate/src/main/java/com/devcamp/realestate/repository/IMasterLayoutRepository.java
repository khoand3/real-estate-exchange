package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.MasterLayout;

public interface IMasterLayoutRepository extends JpaRepository<MasterLayout, Integer> {

}
