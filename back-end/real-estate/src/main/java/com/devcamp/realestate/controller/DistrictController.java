package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.model.DistrictWithProvince;
import com.devcamp.realestate.service.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class DistrictController {
   @Autowired
   private DistrictService districtService;

   @GetMapping("/districts")
   public ResponseEntity<List<District>> getAllDistrict() {
      try {
         return new ResponseEntity<>(districtService.getAllDistrict(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/provinces/{provinceId}/districts")
   public ResponseEntity<List<District>> getAllDistrictByProvinceId(@PathVariable("provinceId") int provinceId) {
      try {
         return new ResponseEntity<>(districtService.getDistrictByProvinceId(provinceId), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/districts-provinces")
   public ResponseEntity<List<DistrictWithProvince>> getAllDistrictWithProvince() {
      try {
         return new ResponseEntity<>(districtService.getAllDistrictWithProvince(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/districts/{id}")
   public ResponseEntity<District> getDistrictById(@PathVariable("id") int id) {
      try {
         District district = districtService.getDistrictById(id);
         if (district != null) {
            return new ResponseEntity<>(district, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/provinces/{provinceId}/districts")
   public ResponseEntity<District> createDistrict(@PathVariable("provinceId") int provinceId,
         @RequestBody District pDistrict) {
      try {
         District newDistrict = districtService.createDistrict(provinceId, pDistrict);
         if (newDistrict != null) {
            return new ResponseEntity<>(newDistrict, HttpStatus.CREATED);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/districts/{id}")
   public ResponseEntity<District> updateDistrictById(@PathVariable("id") int id,
         @RequestBody District pDistrict) {
      try {
         District updateDistrict = districtService.updateDistrict(id, pDistrict);
         if (updateDistrict != null) {
            return new ResponseEntity<>(updateDistrict, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/districts/{id}")
   public ResponseEntity<Object> deleteDistrictById(@PathVariable("id") int id) {
      try {
         Object deleteDistrict = districtService.deleteDistrictById(id);
         if (deleteDistrict != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
