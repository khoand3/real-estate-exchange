package com.devcamp.realestate.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "district")
public class District {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "_name")
   private String name;

   @Column(name = "_prefix")
   private String prefix;

   @ManyToOne
   @JoinColumn(name = "_province_id")
   private Province province;

   @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<Ward> wards;

   @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<Street> streets;

   @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<Project> projects;

   @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<RealEstate> realEstates;

   public District() {
   }

   public District(int id, String name, String prefix, Province province, Set<Ward> wards, Set<Street> streets,
         Set<Project> projects, Set<RealEstate> realEstates) {
      this.id = id;
      this.name = name;
      this.prefix = prefix;
      this.province = province;
      this.wards = wards;
      this.streets = streets;
      this.projects = projects;
      this.realEstates = realEstates;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getPrefix() {
      return prefix;
   }

   public void setPrefix(String prefix) {
      this.prefix = prefix;
   }

   public Province getProvince() {
      return province;
   }

   public void setProvince(Province province) {
      this.province = province;
   }

   public Set<Ward> getWards() {
      return wards;
   }

   public void setWards(Set<Ward> wards) {
      this.wards = wards;
   }

   public Set<Street> getStreets() {
      return streets;
   }

   public void setStreets(Set<Street> streets) {
      this.streets = streets;
   }

   public Set<Project> getProjects() {
      return projects;
   }

   public void setProjects(Set<Project> projects) {
      this.projects = projects;
   }

   public Set<RealEstate> getRealEstates() {
      return realEstates;
   }

   public void setRealEstates(Set<RealEstate> realEstates) {
      this.realEstates = realEstates;
   }
}
