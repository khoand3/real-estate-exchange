package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.RegionLink;
import com.devcamp.realestate.service.RegionLinkService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class RegionLinkController {
   @Autowired
   private RegionLinkService regionLinkService;

   @GetMapping("/region-links")
   public ResponseEntity<List<RegionLink>> getAllRegionLink() {
      try {
         return new ResponseEntity<>(regionLinkService.getAllRegionLink(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/region-links/{id}")
   public ResponseEntity<RegionLink> getRegionLinkById(@PathVariable("id") int id) {
      try {
         RegionLink regionLink = regionLinkService.getRegionLinkById(id);
         if (regionLink != null) {
            return new ResponseEntity<>(regionLink, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/region-links")
   public ResponseEntity<RegionLink> createRegionLink(@RequestBody RegionLink pRegionLink) {
      try {
         return new ResponseEntity<>(regionLinkService.createRegionLink(pRegionLink), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/region-links/{id}")
   public ResponseEntity<RegionLink> updateRegionLinkById(@PathVariable("id") int id,
         @RequestBody RegionLink pRegionLink) {
      try {
         RegionLink updateRegionLink = regionLinkService.updateRegionLink(id, pRegionLink);
         if (updateRegionLink != null) {
            return new ResponseEntity<>(updateRegionLink, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/region-links/{id}")
   public ResponseEntity<Object> deleteRegionLinkById(@PathVariable("id") int id) {
      try {
         Object deleteRegionLink = regionLinkService.deleteRegionLinkById(id);
         if (deleteRegionLink != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
