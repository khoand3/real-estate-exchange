package com.devcamp.realestate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.MasterLayout;
import com.devcamp.realestate.entity.Project;
import com.devcamp.realestate.repository.IProjectRepository;
import com.devcamp.realestate.service.MasterLayoutService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class MasterLayoutController {
   @Autowired
   private MasterLayoutService masterLayoutService;

   @Autowired
   private IProjectRepository projectRepository;

   @GetMapping("/master-layouts")
   public ResponseEntity<List<MasterLayout>> getAllMasterLayout() {
      try {
         return new ResponseEntity<>(masterLayoutService.getAllMasterLayout(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/master-layouts/{id}")
   public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable("id") int id) {
      try {
         MasterLayout masterLayout = masterLayoutService.getMasterLayoutById(id);
         if (masterLayout != null) {
            return new ResponseEntity<>(masterLayout, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/projects/{projectId}/master-layouts")
   public ResponseEntity<MasterLayout> createMasterLayout(@PathVariable("projectId") int projectId,
         @RequestBody MasterLayout pMasterLayout) {
      try {
         Optional<Project> projectData = projectRepository.findById(projectId);
         return new ResponseEntity<>(masterLayoutService.createMasterLayout(pMasterLayout, projectData),
               HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/master-layouts/{id}")
   public ResponseEntity<MasterLayout> updateMasterLayoutById(@PathVariable("id") int id,
         @RequestBody MasterLayout pMasterLayout) {
      try {
         MasterLayout updateMasterLayout = masterLayoutService.updateMasterLayout(id, pMasterLayout);
         if (updateMasterLayout != null) {
            return new ResponseEntity<>(updateMasterLayout, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/master-layouts/{id}")
   public ResponseEntity<Object> deleteMasterLayoutById(@PathVariable("id") int id) {
      try {
         Object deleteMasterLayout = masterLayoutService.deleteMasterLayoutById(id);
         if (deleteMasterLayout != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
