package com.devcamp.realestate.model;

public interface DistrictWithProvince {
   Integer getDistrictId();

   String getDistrictName();

   String getDistrictPrefix();

   String getProvinceName();

   Integer getProvinceId();
}
