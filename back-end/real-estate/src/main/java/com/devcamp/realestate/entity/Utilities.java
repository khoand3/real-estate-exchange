package com.devcamp.realestate.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "utilities")

public class Utilities {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   @Column(nullable = false)
   private String name;
   private String description;
   private String photo;

   @OneToMany(mappedBy = "utilities", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<Project> projects;

   public Utilities() {
   }

   public Utilities(int id, String name, String description, String photo, Set<Project> projects) {
      this.id = id;
      this.name = name;
      this.description = description;
      this.photo = photo;
      this.projects = projects;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getPhoto() {
      return photo;
   }

   public void setPhoto(String photo) {
      this.photo = photo;
   }

   public Set<Project> getProjects() {
      return projects;
   }

   public void setProjects(Set<Project> projects) {
      this.projects = projects;
   }

}
