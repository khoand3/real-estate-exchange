package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Investor;
import com.devcamp.realestate.service.InvestorService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class InvestorController {
   @Autowired
   private InvestorService investorService;

   @GetMapping("/investors")
   public ResponseEntity<List<Investor>> getAllInvestor() {
      try {
         return new ResponseEntity<>(investorService.getAllInvestor(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/investors/{id}")
   public ResponseEntity<Investor> getInvestorById(@PathVariable("id") int id) {
      try {
         Investor investor = investorService
               .getInvestorById(id);
         if (investor != null) {
            return new ResponseEntity<>(investor, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/investors")
   public ResponseEntity<Investor> createInvestor(
         @RequestBody Investor pInvestor) {
      try {
         return new ResponseEntity<>(
               investorService.createInvestor(pInvestor), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/investors/{id}")
   public ResponseEntity<Investor> updateInvestorById(@PathVariable("id") int id,
         @RequestBody Investor pInvestor) {
      try {
         Investor updateInvestor = investorService
               .updateInvestor(id, pInvestor);
         if (updateInvestor != null) {
            return new ResponseEntity<>(updateInvestor, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/investors/{id}")
   public ResponseEntity<Object> deleteInvestorById(@PathVariable("id") int id) {
      try {
         Object deleteInvestor = investorService.deleteInvestorById(id);
         if (deleteInvestor != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
