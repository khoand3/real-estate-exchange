package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Subscription;

public interface ISubscriptionRepository extends JpaRepository<Subscription, Integer> {

}
