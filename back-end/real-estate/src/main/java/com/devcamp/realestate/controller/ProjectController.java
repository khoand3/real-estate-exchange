package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Project;
import com.devcamp.realestate.service.ProjectService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProjectController {
   @Autowired
   private ProjectService projectService;

   @GetMapping("/projects")
   public ResponseEntity<List<Project>> getAllProject() {
      try {
         return new ResponseEntity<>(projectService.getAllProject(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/projects/{id}")
   public ResponseEntity<Project> getProjectById(@PathVariable("id") int id) {
      try {
         Project project = projectService.getProjectById(id);
         if (project != null) {
            return new ResponseEntity<>(project, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/projects/{provinceId}/{districtId}/{investorId}/{constructionContractorId}/{designUnitId}/{utilitiesId}/{regionLinkId}")
   public ResponseEntity<Project> createProject(@PathVariable("provinceId") int provinceId,
         @PathVariable("districtId") int districtId, @PathVariable("investorId") int investorId,
         @PathVariable("constructionContractorId") int constructionContractorId,
         @PathVariable("designUnitId") int designUnitId, @PathVariable("utilitiesId") int utilitiesId,
         @PathVariable("regionLinkId") int regionLinkId,
         @RequestBody Project pProject) {
      try {
         return new ResponseEntity<>(
               projectService.createProject(pProject, provinceId, districtId, investorId, constructionContractorId,
                     designUnitId, utilitiesId, regionLinkId),
               HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/projects/{id}/{provinceId}/{districtId}/{investorId}/{constructionContractorId}/{designUnitId}/{utilitiesId}/{regionLinkId}")
   public ResponseEntity<Project> updateProjectById(@PathVariable("id") int id,
         @PathVariable("provinceId") int provinceId,
         @PathVariable("districtId") int districtId, @PathVariable("investorId") int investorId,
         @PathVariable("constructionContractorId") int constructionContractorId,
         @PathVariable("designUnitId") int designUnitId, @PathVariable("utilitiesId") int utilitiesId,
         @PathVariable("regionLinkId") int regionLinkId,
         @RequestBody Project pProject) {
      try {
         Project updateProject = projectService.updateProject(id, pProject, provinceId, districtId, investorId,
               constructionContractorId,
               designUnitId, utilitiesId, regionLinkId);
         if (updateProject != null) {
            return new ResponseEntity<>(updateProject, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/projects/{id}")
   public ResponseEntity<Object> deleteProjectById(@PathVariable("id") int id) {
      try {
         Object deleteProject = projectService.deleteProjectById(id);
         if (deleteProject != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
