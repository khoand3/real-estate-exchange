package com.devcamp.realestate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "address_map")
public class AddressMap {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   @Column(nullable = false)
   private String address;
   @Column(name = "_lat", nullable = false)
   private double latitude;
   @Column(name = "_lng", nullable = false)
   private double longitude;

   public AddressMap() {
   }

   public AddressMap(int id, String address, double latitude, double longitude) {
      this.id = id;
      this.address = address;
      this.latitude = latitude;
      this.longitude = longitude;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public double getLatitude() {
      return latitude;
   }

   public void setLatitude(double latitude) {
      this.latitude = latitude;
   }

   public double getLongitude() {
      return longitude;
   }

   public void setLongitude(double longitude) {
      this.longitude = longitude;
   }

}
