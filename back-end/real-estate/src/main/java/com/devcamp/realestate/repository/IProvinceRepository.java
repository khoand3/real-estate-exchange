package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Province;

public interface IProvinceRepository extends JpaRepository<Province, Integer> {

}
