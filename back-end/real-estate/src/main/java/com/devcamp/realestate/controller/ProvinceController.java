package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.service.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
   @Autowired
   private ProvinceService provinceService;

   @GetMapping("/provinces")
   public ResponseEntity<List<Province>> getAllProvince() {
      try {
         return new ResponseEntity<>(provinceService.getAllProvince(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/provinces/{id}")
   public ResponseEntity<Province> getProvinceById(@PathVariable("id") int id) {
      try {
         Province province = provinceService.getProvinceById(id);
         if (province != null) {
            return new ResponseEntity<>(province, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/provinces")
   public ResponseEntity<Province> createProvince(@RequestBody Province pProvince) {
      try {
         return new ResponseEntity<>(provinceService.createProvince(pProvince), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/provinces/{id}")
   public ResponseEntity<Province> updateProvinceById(@PathVariable("id") int id,
         @RequestBody Province pProvince) {
      try {
         Province updateProvince = provinceService.updateProvince(id, pProvince);
         if (updateProvince != null) {
            return new ResponseEntity<>(updateProvince, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/provinces/{id}")
   public ResponseEntity<Object> deleteProvinceById(@PathVariable("id") int id) {
      try {
         Object deleteProvince = provinceService.deleteProvinceById(id);
         if (deleteProvince != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
