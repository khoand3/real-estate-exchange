package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Utilities;
import com.devcamp.realestate.repository.IUtilitiesRepository;

@Service
public class UtilitiesService {
   @Autowired
   private IUtilitiesRepository utilitiesRepository;

   public List<Utilities> getAllUtilities() {
      return utilitiesRepository.findAll();
   }

   public Utilities getUtilitiesById(int id) {
      Optional<Utilities> utilitiesData = utilitiesRepository.findById(id);
      if (utilitiesData.isPresent()) {
         return utilitiesData.get();
      } else {
         return null;
      }
   }

   public Utilities createUtilities(Utilities pUtilities) {
      Utilities newUtilities = new Utilities();

      newUtilities.setName(pUtilities.getName());
      newUtilities.setDescription(pUtilities.getDescription());
      newUtilities.setPhoto(pUtilities.getPhoto());

      utilitiesRepository.save(newUtilities);

      return newUtilities;
   }

   public Utilities updateUtilities(int id, Utilities pUtilities) {
      Optional<Utilities> utilitiesData = utilitiesRepository.findById(id);
      if (utilitiesData.isPresent()) {
         Utilities updateUtilities = utilitiesData.get();

         updateUtilities.setName(pUtilities.getName());
         updateUtilities.setDescription(pUtilities.getDescription());
         updateUtilities.setPhoto(pUtilities.getPhoto());

         utilitiesRepository.save(updateUtilities);

         return updateUtilities;
      } else {
         return null;
      }
   }

   public Object deleteUtilitiesById(int id) {
      Optional<Utilities> utilitiesData = utilitiesRepository.findById(id);
      if (utilitiesData.isPresent()) {
         utilitiesRepository.deleteById(id);
         return utilitiesData;
      } else {
         return null;
      }
   }
}
