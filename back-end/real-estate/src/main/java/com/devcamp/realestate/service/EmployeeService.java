package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Employee;
import com.devcamp.realestate.repository.IEmployeeRepository;

@Service
public class EmployeeService {
   @Autowired
   private IEmployeeRepository employeeRepository;

   public List<Employee> getAllEmployee() {
      return employeeRepository.findAll();
   }

   public Employee getEmployeeById(int id) {
      Optional<Employee> employeeData = employeeRepository.findById(id);
      if (employeeData.isPresent()) {
         return employeeData.get();
      } else {
         return null;
      }
   }

   public Employee createEmployee(Employee pEmployee) {
      Employee newEmployee = new Employee();

      newEmployee.setLastName(pEmployee.getLastName());
      newEmployee.setFirstName(pEmployee.getFirstName());
      newEmployee.setTitle(pEmployee.getTitle());
      newEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
      newEmployee.setBirthDate(pEmployee.getBirthDate());
      newEmployee.setHireDate(pEmployee.getHireDate());
      newEmployee.setAddress(pEmployee.getAddress());
      newEmployee.setCity(pEmployee.getCity());
      newEmployee.setRegion(pEmployee.getRegion());
      newEmployee.setPostalCode(pEmployee.getPostalCode());
      newEmployee.setCountry(pEmployee.getCountry());
      newEmployee.setHomePhone(pEmployee.getHomePhone());
      newEmployee.setExtension(pEmployee.getExtension());
      newEmployee.setPhoto(pEmployee.getPhoto());
      newEmployee.setNotes(pEmployee.getNotes());
      newEmployee.setReportsTo(pEmployee.getReportsTo());
      newEmployee.setUsername(pEmployee.getUsername());
      newEmployee.setPassword(pEmployee.getPassword());
      newEmployee.setEmail(pEmployee.getEmail());
      newEmployee.setActivated(pEmployee.getActivated());
      newEmployee.setProfile(pEmployee.getProfile());
      newEmployee.setUserLevel(pEmployee.getUserLevel());

      employeeRepository.save(newEmployee);

      return newEmployee;
   }

   public Employee updateEmployee(int id, Employee pEmployee) {
      Optional<Employee> employeeData = employeeRepository.findById(id);
      if (employeeData.isPresent()) {
         Employee updateEmployee = employeeData.get();

         updateEmployee.setLastName(pEmployee.getLastName());
         updateEmployee.setFirstName(pEmployee.getFirstName());
         updateEmployee.setTitle(pEmployee.getTitle());
         updateEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
         updateEmployee.setBirthDate(pEmployee.getBirthDate());
         updateEmployee.setHireDate(pEmployee.getHireDate());
         updateEmployee.setAddress(pEmployee.getAddress());
         updateEmployee.setCity(pEmployee.getCity());
         updateEmployee.setRegion(pEmployee.getRegion());
         updateEmployee.setPostalCode(pEmployee.getPostalCode());
         updateEmployee.setCountry(pEmployee.getCountry());
         updateEmployee.setHomePhone(pEmployee.getHomePhone());
         updateEmployee.setExtension(pEmployee.getExtension());
         updateEmployee.setPhoto(pEmployee.getPhoto());
         updateEmployee.setNotes(pEmployee.getNotes());
         updateEmployee.setReportsTo(pEmployee.getReportsTo());
         updateEmployee.setUsername(pEmployee.getUsername());
         updateEmployee.setPassword(pEmployee.getPassword());
         updateEmployee.setEmail(pEmployee.getEmail());
         updateEmployee.setActivated(pEmployee.getActivated());
         updateEmployee.setProfile(pEmployee.getProfile());
         updateEmployee.setUserLevel(pEmployee.getUserLevel());

         employeeRepository.save(updateEmployee);

         return updateEmployee;
      } else {
         return null;
      }
   }

   public Object deleteEmployeeById(int id) {
      Optional<Employee> employeeData = employeeRepository.findById(id);
      if (employeeData.isPresent()) {
         employeeRepository.deleteById(id);
         return employeeData;
      } else {
         return null;
      }
   }
}
