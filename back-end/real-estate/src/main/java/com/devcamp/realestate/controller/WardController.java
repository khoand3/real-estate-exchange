package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Ward;
import com.devcamp.realestate.service.WardService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class WardController {
   @Autowired
   private WardService wardService;

   @GetMapping("/wards")
   public ResponseEntity<List<Ward>> getAllWard() {
      try {
         return new ResponseEntity<>(wardService.getAllWard(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/wards-all")
   public ResponseEntity<List<Ward>> getAllWards() {
      try {
         return new ResponseEntity<>(wardService.getAllWards(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/wards/{id}")
   public ResponseEntity<Ward> getWardById(@PathVariable("id") int id) {
      try {
         Ward ward = wardService.getWardById(id);
         if (ward != null) {
            return new ResponseEntity<>(ward, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("provinces/{provincesId}/districts/{districtId}/wards")
   public ResponseEntity<Ward> createWard(@PathVariable("provincesId") int provincesId,
         @PathVariable("districtId") int districtId,
         @RequestBody Ward pWard) {
      try {
         Ward newWard = wardService.createWard(provincesId, districtId, pWard);
         if (newWard != null) {
            return new ResponseEntity<>(newWard, HttpStatus.CREATED);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/wards/{id}")
   public ResponseEntity<Ward> updateWardById(@PathVariable("id") int id,
         @RequestBody Ward pWard) {
      try {
         Ward updateWard = wardService.updateWard(id, pWard);
         if (updateWard != null) {
            return new ResponseEntity<>(updateWard, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/wards/{id}")
   public ResponseEntity<Object> deleteWardById(@PathVariable("id") int id) {
      try {
         Object deleteWard = wardService.deleteWardById(id);
         if (deleteWard != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
