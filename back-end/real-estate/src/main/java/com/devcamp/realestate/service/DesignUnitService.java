package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.DesignUnit;
import com.devcamp.realestate.repository.IDesignUnitRepository;

@Service
public class DesignUnitService {
   @Autowired
   private IDesignUnitRepository designUnitRepository;

   public List<DesignUnit> getAllDesignUnit() {
      return designUnitRepository.findAll();
   }

   public DesignUnit getDesignUnitById(int id) {
      Optional<DesignUnit> designUnitData = designUnitRepository.findById(id);
      if (designUnitData.isPresent()) {
         return designUnitData.get();
      } else {
         return null;
      }
   }

   public DesignUnit createDesignUnit(DesignUnit pDesignUnit) {
      DesignUnit newDesignUnit = new DesignUnit();

      newDesignUnit.setName(pDesignUnit.getName());
      newDesignUnit.setDescription(pDesignUnit.getDescription());
      newDesignUnit.setProjects(pDesignUnit.getProjects());
      newDesignUnit.setAddress(pDesignUnit.getAddress());
      newDesignUnit.setPhone(pDesignUnit.getPhone());
      newDesignUnit.setPhone2(pDesignUnit.getPhone2());
      newDesignUnit.setFax(pDesignUnit.getFax());
      newDesignUnit.setEmail(pDesignUnit.getEmail());
      newDesignUnit.setWebsite(pDesignUnit.getWebsite());
      newDesignUnit.setNote(pDesignUnit.getNote());

      designUnitRepository.save(newDesignUnit);

      return newDesignUnit;
   }

   public DesignUnit updateDesignUnit(int id, DesignUnit pDesignUnit) {
      Optional<DesignUnit> designUnitData = designUnitRepository.findById(id);
      if (designUnitData.isPresent()) {
         DesignUnit updateDesignUnit = designUnitData.get();

         updateDesignUnit.setName(pDesignUnit.getName());
         updateDesignUnit.setDescription(pDesignUnit.getDescription());
         updateDesignUnit.setProjects(pDesignUnit.getProjects());
         updateDesignUnit.setAddress(pDesignUnit.getAddress());
         updateDesignUnit.setPhone(pDesignUnit.getPhone());
         updateDesignUnit.setPhone2(pDesignUnit.getPhone2());
         updateDesignUnit.setFax(pDesignUnit.getFax());
         updateDesignUnit.setEmail(pDesignUnit.getEmail());
         updateDesignUnit.setWebsite(pDesignUnit.getWebsite());
         updateDesignUnit.setNote(pDesignUnit.getNote());

         designUnitRepository.save(updateDesignUnit);

         return updateDesignUnit;
      } else {
         return null;
      }
   }

   public Object deleteDesignUnitById(int id) {
      Optional<DesignUnit> designUnitData = designUnitRepository.findById(id);
      if (designUnitData.isPresent()) {
         designUnitRepository.deleteById(id);
         return designUnitData;
      } else {
         return null;
      }
   }
}
