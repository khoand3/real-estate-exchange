package com.devcamp.realestate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subscriptions")

public class Subscription {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   private String user;
   @Column(nullable = false)
   private String endpoint;
   @Column(nullable = false)
   private String publickey;
   @Column(name = "authenticationtoken", nullable = false)
   private String authenticationToken;
   @Column(name = "contentencoding", nullable = false)
   private String contentEncoding;

   public Subscription() {
   }

   public Subscription(int id, String user, String endpoint, String publickey, String authenticationToken,
         String contentEncoding) {
      this.id = id;
      this.user = user;
      this.endpoint = endpoint;
      this.publickey = publickey;
      this.authenticationToken = authenticationToken;
      this.contentEncoding = contentEncoding;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getUser() {
      return user;
   }

   public void setUser(String user) {
      this.user = user;
   }

   public String getEndpoint() {
      return endpoint;
   }

   public void setEndpoint(String endpoint) {
      this.endpoint = endpoint;
   }

   public String getPublickey() {
      return publickey;
   }

   public void setPublickey(String publickey) {
      this.publickey = publickey;
   }

   public String getAuthenticationToken() {
      return authenticationToken;
   }

   public void setAuthenticationToken(String authenticationToken) {
      this.authenticationToken = authenticationToken;
   }

   public String getContentEncoding() {
      return contentEncoding;
   }

   public void setContentEncoding(String contentEncoding) {
      this.contentEncoding = contentEncoding;
   }

}
