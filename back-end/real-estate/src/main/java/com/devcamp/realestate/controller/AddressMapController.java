package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.AddressMap;
import com.devcamp.realestate.service.AddressMapService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class AddressMapController {
   @Autowired
   private AddressMapService addressMapService;

   @GetMapping("/address-maps")
   public ResponseEntity<List<AddressMap>> getAllAddressMap() {
      try {
         return new ResponseEntity<>(addressMapService.getAllAddressMap(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/address-maps/{id}")
   public ResponseEntity<AddressMap> getAddressMapById(@PathVariable("id") int id) {
      try {
         AddressMap addressMap = addressMapService.getAddressMapById(id);
         if (addressMap != null) {
            return new ResponseEntity<>(addressMap, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/address-maps")
   public ResponseEntity<AddressMap> createAddressMap(@RequestBody AddressMap pAddressMap) {
      try {
         return new ResponseEntity<>(addressMapService.createAddressMap(pAddressMap), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/address-maps/{id}")
   public ResponseEntity<AddressMap> updateAddressMapById(@PathVariable("id") int id,
         @RequestBody AddressMap pAddressMap) {
      try {
         AddressMap updateAddressMap = addressMapService.updateAddressMap(id, pAddressMap);
         if (updateAddressMap != null) {
            return new ResponseEntity<>(updateAddressMap, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/address-maps/{id}")
   public ResponseEntity<Object> deleteAddressMapById(@PathVariable("id") int id) {
      try {
         Object deleteAddressMap = addressMapService.deleteAddressMapById(id);
         if (deleteAddressMap != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
