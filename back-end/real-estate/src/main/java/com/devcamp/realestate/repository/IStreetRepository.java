package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Street;

public interface IStreetRepository extends JpaRepository<Street, Integer> {

}
