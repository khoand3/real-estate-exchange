package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.RegionLink;
import com.devcamp.realestate.repository.IRegionLinkRepository;

@Service
public class RegionLinkService {
   @Autowired
   private IRegionLinkRepository regionLinkRepository;

   public List<RegionLink> getAllRegionLink() {
      return regionLinkRepository.findAll();
   }

   public RegionLink getRegionLinkById(int id) {
      Optional<RegionLink> regionLinkData = regionLinkRepository.findById(id);
      if (regionLinkData.isPresent()) {
         return regionLinkData.get();
      } else {
         return null;
      }
   }

   public RegionLink createRegionLink(RegionLink pRegionLink) {
      RegionLink newRegionLink = new RegionLink();

      newRegionLink.setName(pRegionLink.getName());
      newRegionLink.setDescription(pRegionLink.getDescription());
      newRegionLink.setPhoto(pRegionLink.getPhoto());
      newRegionLink.setAddress(pRegionLink.getAddress());
      newRegionLink.setLatitude(pRegionLink.getLatitude());
      newRegionLink.setLongitude(pRegionLink.getLongitude());

      regionLinkRepository.save(newRegionLink);

      return newRegionLink;
   }

   public RegionLink updateRegionLink(int id, RegionLink pRegionLink) {
      Optional<RegionLink> regionLinkData = regionLinkRepository.findById(id);
      if (regionLinkData.isPresent()) {
         RegionLink updateRegionLink = regionLinkData.get();

         updateRegionLink.setName(pRegionLink.getName());
         updateRegionLink.setDescription(pRegionLink.getDescription());
         updateRegionLink.setPhoto(pRegionLink.getPhoto());
         updateRegionLink.setAddress(pRegionLink.getAddress());
         updateRegionLink.setLatitude(pRegionLink.getLatitude());
         updateRegionLink.setLongitude(pRegionLink.getLongitude());

         regionLinkRepository.save(updateRegionLink);

         return updateRegionLink;
      } else {
         return null;
      }
   }

   public Object deleteRegionLinkById(int id) {
      Optional<RegionLink> regionLinkData = regionLinkRepository.findById(id);
      if (regionLinkData.isPresent()) {
         regionLinkRepository.deleteById(id);
         return regionLinkData;
      } else {
         return null;
      }
   }
}
