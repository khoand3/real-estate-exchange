package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Location;
import com.devcamp.realestate.repository.ILocationRepository;

@Service
public class LocationService {
   @Autowired
   private ILocationRepository locationRepository;

   public List<Location> getAllLocation() {
      return locationRepository.findAll();
   }

   public Location getLocationById(int id) {
      Optional<Location> locationData = locationRepository.findById(id);
      if (locationData.isPresent()) {
         return locationData.get();
      } else {
         return null;
      }
   }

   public Location createLocation(Location pLocation) {
      Location newLocation = new Location();

      newLocation.setLatitude(pLocation.getLatitude());
      newLocation.setLongitude(pLocation.getLongitude());

      locationRepository.save(newLocation);

      return newLocation;
   }

   public Location updateLocation(int id, Location pLocation) {
      Optional<Location> locationData = locationRepository.findById(id);
      if (locationData.isPresent()) {
         Location updateLocation = locationData.get();

         updateLocation.setLatitude(pLocation.getLatitude());
         updateLocation.setLongitude(pLocation.getLongitude());
         locationRepository.save(updateLocation);

         return updateLocation;
      } else {
         return null;
      }
   }

   public Object deleteLocationById(int id) {
      Optional<Location> locationData = locationRepository.findById(id);
      if (locationData.isPresent()) {
         locationRepository.deleteById(id);
         return locationData;
      } else {
         return null;
      }
   }
}
