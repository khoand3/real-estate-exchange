package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.ConstructionContractor;
import com.devcamp.realestate.service.ConstructionContractorService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ConstructionContractorController {
   @Autowired
   private ConstructionContractorService constructionContractorService;

   @GetMapping("/construction-contractors")
   public ResponseEntity<List<ConstructionContractor>> getAllConstructionContractor() {
      try {
         return new ResponseEntity<>(constructionContractorService.getAllConstructionContractor(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/construction-contractors/{id}")
   public ResponseEntity<ConstructionContractor> getConstructionContractorById(@PathVariable("id") int id) {
      try {
         ConstructionContractor constructionContractor = constructionContractorService
               .getConstructionContractorById(id);
         if (constructionContractor != null) {
            return new ResponseEntity<>(constructionContractor, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/construction-contractors")
   public ResponseEntity<ConstructionContractor> createConstructionContractor(
         @RequestBody ConstructionContractor pConstructionContractor) {
      try {
         return new ResponseEntity<>(
               constructionContractorService.createConstructionContractor(pConstructionContractor), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/construction-contractors/{id}")
   public ResponseEntity<ConstructionContractor> updateConstructionContractorById(@PathVariable("id") int id,
         @RequestBody ConstructionContractor pConstructionContractor) {
      try {
         ConstructionContractor updateConstructionContractor = constructionContractorService
               .updateConstructionContractor(id, pConstructionContractor);
         if (updateConstructionContractor != null) {
            return new ResponseEntity<>(updateConstructionContractor, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/construction-contractors/{id}")
   public ResponseEntity<Object> deleteConstructionContractorById(@PathVariable("id") int id) {
      try {
         Object deleteConstructionContractor = constructionContractorService.deleteConstructionContractorById(id);
         if (deleteConstructionContractor != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
