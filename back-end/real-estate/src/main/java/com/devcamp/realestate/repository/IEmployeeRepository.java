package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee, Integer> {

}
