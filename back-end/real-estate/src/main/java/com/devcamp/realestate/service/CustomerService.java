package com.devcamp.realestate.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Customer;
import com.devcamp.realestate.repository.ICustomerRepository;

@Service
public class CustomerService {
   @Autowired
   private ICustomerRepository customerRepository;

   public List<Customer> getAllCustomer() {
      return customerRepository.findAll();
   }

   public Customer getCustomerById(int id) {
      Optional<Customer> customerData = customerRepository.findById(id);
      if (customerData.isPresent()) {
         return customerData.get();
      } else {
         return null;
      }
   }

   public Customer createCustomer(Customer pCustomer) {
      Customer newCustomer = new Customer();

      newCustomer.setName(pCustomer.getName());
      newCustomer.setContactTitle(pCustomer.getContactTitle());
      newCustomer.setAddress(pCustomer.getAddress());
      newCustomer.setMobile(pCustomer.getMobile());
      newCustomer.setEmail(pCustomer.getEmail());
      newCustomer.setNote(pCustomer.getNote());
      newCustomer.setCreateBy(pCustomer.getCreateBy());
      newCustomer.setCreateDate(new Date());
      newCustomer.setUpdateDate(null);

      customerRepository.save(newCustomer);

      return newCustomer;
   }

   public Customer updateCustomer(int id, Customer pCustomer) {
      Optional<Customer> customerData = customerRepository.findById(id);
      if (customerData.isPresent()) {
         Customer updateCustomer = customerData.get();

         updateCustomer.setName(pCustomer.getName());
         updateCustomer.setContactTitle(pCustomer.getContactTitle());
         updateCustomer.setAddress(pCustomer.getAddress());
         updateCustomer.setMobile(pCustomer.getMobile());
         updateCustomer.setEmail(pCustomer.getEmail());
         updateCustomer.setNote(pCustomer.getNote());
         updateCustomer.setUpdateBy(pCustomer.getUpdateBy());
         updateCustomer.setUpdateDate(new Date());

         customerRepository.save(updateCustomer);

         return updateCustomer;
      } else {
         return null;
      }
   }

   public Object deleteCustomerById(int id) {
      Optional<Customer> customerData = customerRepository.findById(id);
      if (customerData.isPresent()) {
         customerRepository.deleteById(id);
         return customerData;
      } else {
         return null;
      }
   }
}
