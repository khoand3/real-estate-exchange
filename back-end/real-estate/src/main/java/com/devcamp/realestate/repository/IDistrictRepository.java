package com.devcamp.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.model.DistrictWithProvince;

public interface IDistrictRepository extends JpaRepository<District, Integer> {
   @Query(value = "SELECT district.id AS districtId, district._name AS districtName, district._prefix AS districtPrefix, district._province_id AS provinceId, province._name AS provinceName FROM district JOIN province ON district._province_id = province.id", nativeQuery = true)
   List<DistrictWithProvince> getAllDistrictWithProvince();

   List<District> findByProvinceId(int provinceId);
}
