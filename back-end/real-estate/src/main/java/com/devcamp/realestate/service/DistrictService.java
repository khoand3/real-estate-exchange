package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.model.DistrictWithProvince;
import com.devcamp.realestate.repository.IDistrictRepository;
import com.devcamp.realestate.repository.IProvinceRepository;

@Service
public class DistrictService {
   @Autowired
   private IDistrictRepository districtRepository;

   @Autowired
   private IProvinceRepository provinceRepository;

   public List<District> getAllDistrict() {
      return districtRepository.findAll();
   }

   public List<DistrictWithProvince> getAllDistrictWithProvince() {
      return districtRepository.getAllDistrictWithProvince();
   }

   public District getDistrictById(int id) {
      Optional<District> districtData = districtRepository.findById(id);
      if (districtData.isPresent()) {
         return districtData.get();
      } else {
         return null;
      }
   }

   public List<District> getDistrictByProvinceId(int id) {
      return districtRepository.findByProvinceId(id);
   }

   public District createDistrict(int provinceId, District pDistrict) {
      Optional<Province> provinceData = provinceRepository.findById(provinceId);
      if (provinceData.isPresent()) {
         District newDistrict = new District();

         newDistrict.setPrefix(pDistrict.getPrefix());
         newDistrict.setName(pDistrict.getName());
         newDistrict.setProvince(provinceData.get());

         districtRepository.save(newDistrict);

         return newDistrict;
      } else {
         return null;
      }
   }

   public District updateDistrict(int id, District pDistrict) {
      Optional<District> districtData = districtRepository.findById(id);
      if (districtData.isPresent()) {
         District updateDistrict = districtData.get();

         updateDistrict.setPrefix(pDistrict.getPrefix());
         updateDistrict.setName(pDistrict.getName());

         districtRepository.save(updateDistrict);

         return updateDistrict;
      } else {
         return null;
      }
   }

   public Object deleteDistrictById(int id) {
      Optional<District> districtData = districtRepository.findById(id);
      if (districtData.isPresent()) {
         districtRepository.deleteById(id);
         return districtData;
      } else {
         return null;
      }
   }
}
