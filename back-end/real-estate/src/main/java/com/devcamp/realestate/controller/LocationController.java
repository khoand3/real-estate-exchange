package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Location;
import com.devcamp.realestate.service.LocationService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class LocationController {
   @Autowired
   private LocationService locationService;

   @GetMapping("/locations")
   public ResponseEntity<List<Location>> getAllLocation() {
      try {
         return new ResponseEntity<>(locationService.getAllLocation(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/locations/{id}")
   public ResponseEntity<Location> getLocationById(@PathVariable("id") int id) {
      try {
         Location location = locationService.getLocationById(id);
         if (location != null) {
            return new ResponseEntity<>(location, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/locations")
   public ResponseEntity<Location> createLocation(@RequestBody Location pLocation) {
      try {
         return new ResponseEntity<>(locationService.createLocation(pLocation), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/locations/{id}")
   public ResponseEntity<Location> updateLocationById(@PathVariable("id") int id,
         @RequestBody Location pLocation) {
      try {
         Location updateLocation = locationService.updateLocation(id, pLocation);
         if (updateLocation != null) {
            return new ResponseEntity<>(updateLocation, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/locations/{id}")
   public ResponseEntity<Object> deleteLocationById(@PathVariable("id") int id) {
      try {
         Object deleteLocation = locationService.deleteLocationById(id);
         if (deleteLocation != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
