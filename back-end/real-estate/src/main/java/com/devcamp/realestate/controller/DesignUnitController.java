package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.DesignUnit;
import com.devcamp.realestate.service.DesignUnitService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class DesignUnitController {
   @Autowired
   private DesignUnitService designUnitService;

   @GetMapping("/design-units")
   public ResponseEntity<List<DesignUnit>> getAllDesignUnit() {
      try {
         return new ResponseEntity<>(designUnitService.getAllDesignUnit(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/design-units/{id}")
   public ResponseEntity<DesignUnit> getDesignUnitById(@PathVariable("id") int id) {
      try {
         DesignUnit designUnit = designUnitService
               .getDesignUnitById(id);
         if (designUnit != null) {
            return new ResponseEntity<>(designUnit, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/design-units")
   public ResponseEntity<DesignUnit> createDesignUnit(
         @RequestBody DesignUnit pDesignUnit) {
      try {
         return new ResponseEntity<>(
               designUnitService.createDesignUnit(pDesignUnit), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/design-units/{id}")
   public ResponseEntity<DesignUnit> updateDesignUnitById(@PathVariable("id") int id,
         @RequestBody DesignUnit pDesignUnit) {
      try {
         DesignUnit updateDesignUnit = designUnitService
               .updateDesignUnit(id, pDesignUnit);
         if (updateDesignUnit != null) {
            return new ResponseEntity<>(updateDesignUnit, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/design-units/{id}")
   public ResponseEntity<Object> deleteDesignUnitById(@PathVariable("id") int id) {
      try {
         Object deleteDesignUnit = designUnitService.deleteDesignUnitById(id);
         if (deleteDesignUnit != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
