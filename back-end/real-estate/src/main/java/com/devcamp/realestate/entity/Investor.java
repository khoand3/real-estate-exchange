package com.devcamp.realestate.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "investor")
public class Investor {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   @Column(nullable = false)
   private String name;
   private String description;
   private String projects;
   private Integer address;
   private String phone;
   private String phone2;
   private String fax;
   private String email;
   private String website;
   private String note;

   @OneToMany(mappedBy = "investor", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<Project> projects2;

   public Investor() {
   }

   public Investor(int id, String name, String description, String projects, Integer address, String phone,
         String phone2, String fax, String email, String website, String note, Set<Project> projects2) {
      this.id = id;
      this.name = name;
      this.description = description;
      this.projects = projects;
      this.address = address;
      this.phone = phone;
      this.phone2 = phone2;
      this.fax = fax;
      this.email = email;
      this.website = website;
      this.note = note;
      this.projects2 = projects2;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getProjects() {
      return projects;
   }

   public void setProjects(String projects) {
      this.projects = projects;
   }

   public String getPhone() {
      return phone;
   }

   public void setPhone(String phone) {
      this.phone = phone;
   }

   public String getPhone2() {
      return phone2;
   }

   public void setPhone2(String phone2) {
      this.phone2 = phone2;
   }

   public String getFax() {
      return fax;
   }

   public void setFax(String fax) {
      this.fax = fax;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getWebsite() {
      return website;
   }

   public void setWebsite(String website) {
      this.website = website;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public Integer getAddress() {
      return address;
   }

   public void setAddress(Integer address) {
      this.address = address;
   }

   public Set<Project> getProjects2() {
      return projects2;
   }

   public void setProjects2(Set<Project> projects2) {
      this.projects2 = projects2;
   }

}
