package com.devcamp.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.entity.Ward;

public interface IWardRepository extends JpaRepository<Ward, Integer> {
   @Query(value = "SELECT * from ward", nativeQuery = true)
   List<Ward> getAllWards();
}
