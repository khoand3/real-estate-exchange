package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Subscription;
import com.devcamp.realestate.service.SubscriptionService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class SubscriptionController {
   @Autowired
   private SubscriptionService subscriptionService;

   @GetMapping("/subscriptions")
   public ResponseEntity<List<Subscription>> getAllSubscription() {
      try {
         return new ResponseEntity<>(subscriptionService.getAllSubscription(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/subscriptions/{id}")
   public ResponseEntity<Subscription> getSubscriptionById(@PathVariable("id") int id) {
      try {
         Subscription subscription = subscriptionService.getSubscriptionById(id);
         if (subscription != null) {
            return new ResponseEntity<>(subscription, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/subscriptions")
   public ResponseEntity<Subscription> createSubscription(@RequestBody Subscription pSubscription) {
      try {
         return new ResponseEntity<>(subscriptionService.createSubscription(pSubscription), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/subscriptions/{id}")
   public ResponseEntity<Subscription> updateSubscriptionById(@PathVariable("id") int id,
         @RequestBody Subscription pSubscription) {
      try {
         Subscription updateSubscription = subscriptionService.updateSubscription(id, pSubscription);
         if (updateSubscription != null) {
            return new ResponseEntity<>(updateSubscription, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/subscriptions/{id}")
   public ResponseEntity<Object> deleteSubscriptionById(@PathVariable("id") int id) {
      try {
         Object deleteSubscription = subscriptionService.deleteSubscriptionById(id);
         if (deleteSubscription != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
