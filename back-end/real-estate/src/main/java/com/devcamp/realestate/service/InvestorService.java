package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Investor;
import com.devcamp.realestate.repository.IInvestorRepository;

@Service
public class InvestorService {
   @Autowired
   private IInvestorRepository investorRepository;

   public List<Investor> getAllInvestor() {
      return investorRepository.findAll();
   }

   public Investor getInvestorById(int id) {
      Optional<Investor> investorData = investorRepository.findById(id);
      if (investorData.isPresent()) {
         return investorData.get();
      } else {
         return null;
      }
   }

   public Investor createInvestor(Investor pInvestor) {
      Investor newInvestor = new Investor();

      newInvestor.setName(pInvestor.getName());
      newInvestor.setDescription(pInvestor.getDescription());
      newInvestor.setProjects(pInvestor.getProjects());
      newInvestor.setAddress(pInvestor.getAddress());
      newInvestor.setPhone(pInvestor.getPhone());
      newInvestor.setPhone2(pInvestor.getPhone2());
      newInvestor.setFax(pInvestor.getFax());
      newInvestor.setEmail(pInvestor.getEmail());
      newInvestor.setWebsite(pInvestor.getWebsite());
      newInvestor.setNote(pInvestor.getNote());

      investorRepository.save(newInvestor);

      return newInvestor;
   }

   public Investor updateInvestor(int id, Investor pInvestor) {
      Optional<Investor> investorData = investorRepository.findById(id);
      if (investorData.isPresent()) {
         Investor updateInvestor = investorData.get();

         updateInvestor.setName(pInvestor.getName());
         updateInvestor.setDescription(pInvestor.getDescription());
         updateInvestor.setProjects(pInvestor.getProjects());
         updateInvestor.setAddress(pInvestor.getAddress());
         updateInvestor.setPhone(pInvestor.getPhone());
         updateInvestor.setPhone2(pInvestor.getPhone2());
         updateInvestor.setFax(pInvestor.getFax());
         updateInvestor.setEmail(pInvestor.getEmail());
         updateInvestor.setWebsite(pInvestor.getWebsite());
         updateInvestor.setNote(pInvestor.getNote());

         investorRepository.save(updateInvestor);

         return updateInvestor;
      } else {
         return null;
      }
   }

   public Object deleteInvestorById(int id) {
      Optional<Investor> investorData = investorRepository.findById(id);
      if (investorData.isPresent()) {
         investorRepository.deleteById(id);
         return investorData;
      } else {
         return null;
      }
   }
}
