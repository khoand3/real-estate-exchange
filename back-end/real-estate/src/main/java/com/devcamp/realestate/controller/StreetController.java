package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Street;
import com.devcamp.realestate.service.StreetService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class StreetController {
   @Autowired
   private StreetService streetService;

   @GetMapping("/streets")
   public ResponseEntity<List<Street>> getAllStreet() {
      try {
         return new ResponseEntity<>(streetService.getAllStreet(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/streets/{id}")
   public ResponseEntity<Street> getStreetById(@PathVariable("id") int id) {
      try {
         Street street = streetService.getStreetById(id);
         if (street != null) {
            return new ResponseEntity<>(street, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("provinces/{provinceId}/districts/{districtId}/streets")
   public ResponseEntity<Street> createStreet(@PathVariable("provinceId") int provinceId,
         @PathVariable("districtId") int districtId,
         @RequestBody Street pStreet) {
      try {
         Street newStreet = streetService.createStreet(provinceId, districtId, pStreet);
         if (newStreet != null) {
            return new ResponseEntity<>(newStreet, HttpStatus.CREATED);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/streets/{id}")
   public ResponseEntity<Street> updateStreetById(@PathVariable("id") int id,
         @RequestBody Street pStreet) {
      try {
         Street updateStreet = streetService.updateStreet(id, pStreet);
         if (updateStreet != null) {
            return new ResponseEntity<>(updateStreet, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/streets/{id}")
   public ResponseEntity<Object> deleteStreetById(@PathVariable("id") int id) {
      try {
         Object deleteStreet = streetService.deleteStreetById(id);
         if (deleteStreet != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
