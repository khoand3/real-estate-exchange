package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Street;
import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.repository.IStreetRepository;
import com.devcamp.realestate.repository.IDistrictRepository;
import com.devcamp.realestate.repository.IProvinceRepository;

@Service
public class StreetService {
   @Autowired
   private IProvinceRepository provinceRepository;

   @Autowired
   private IDistrictRepository districtRepository;

   @Autowired
   private IStreetRepository streetRepository;

   public List<Street> getAllStreet() {
      return streetRepository.findAll();
   }

   public Street getStreetById(int id) {
      Optional<Street> streetData = streetRepository.findById(id);
      if (streetData.isPresent()) {
         return streetData.get();
      } else {
         return null;
      }
   }

   public Street createStreet(int provinceId, int districtId, Street pStreet) {
      Optional<Province> provinceData = provinceRepository.findById(districtId);
      Optional<District> districtData = districtRepository.findById(districtId);
      if (provinceData.isPresent() && districtData.isPresent()) {
         Street newStreet = new Street();

         newStreet.setPrefix(pStreet.getPrefix());
         newStreet.setName(pStreet.getName());
         newStreet.setDistrict(districtData.get());
         newStreet.setProvince(provinceData.get());

         streetRepository.save(newStreet);

         return newStreet;
      } else {
         return null;
      }
   }

   public Street updateStreet(int id, Street pStreet) {
      Optional<Street> streetData = streetRepository.findById(id);
      if (streetData.isPresent()) {
         Street updateStreet = streetData.get();

         updateStreet.setPrefix(pStreet.getPrefix());
         updateStreet.setName(pStreet.getName());

         streetRepository.save(updateStreet);

         return updateStreet;
      } else {
         return null;
      }
   }

   public Object deleteStreetById(int id) {
      Optional<Street> streetData = streetRepository.findById(id);
      if (streetData.isPresent()) {
         streetRepository.deleteById(id);
         return streetData;
      } else {
         return null;
      }
   }
}
