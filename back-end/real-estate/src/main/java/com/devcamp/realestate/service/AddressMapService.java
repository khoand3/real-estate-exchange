package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.AddressMap;
import com.devcamp.realestate.repository.IAddressMapRepository;

@Service
public class AddressMapService {
   @Autowired
   private IAddressMapRepository addressMapRepository;

   public List<AddressMap> getAllAddressMap() {
      return addressMapRepository.findAll();
   }

   public AddressMap getAddressMapById(int id) {
      Optional<AddressMap> addressMapData = addressMapRepository.findById(id);
      if (addressMapData.isPresent()) {
         return addressMapData.get();
      } else {
         return null;
      }
   }

   public AddressMap createAddressMap(AddressMap pAddressMap) {
      AddressMap newAddressMap = new AddressMap();

      newAddressMap.setAddress(pAddressMap.getAddress());
      newAddressMap.setLatitude(pAddressMap.getLatitude());
      newAddressMap.setLongitude(pAddressMap.getLongitude());

      addressMapRepository.save(newAddressMap);

      return newAddressMap;
   }

   public AddressMap updateAddressMap(int id, AddressMap pAddressMap) {
      Optional<AddressMap> addressMapData = addressMapRepository.findById(id);
      if (addressMapData.isPresent()) {
         AddressMap updateAddressMap = addressMapData.get();

         updateAddressMap.setAddress(pAddressMap.getAddress());
         updateAddressMap.setLatitude(pAddressMap.getLatitude());
         updateAddressMap.setLongitude(pAddressMap.getLongitude());

         addressMapRepository.save(updateAddressMap);

         return updateAddressMap;
      } else {
         return null;
      }
   }

   public Object deleteAddressMapById(int id) {
      Optional<AddressMap> addressMapData = addressMapRepository.findById(id);
      if (addressMapData.isPresent()) {
         addressMapRepository.deleteById(id);
         return addressMapData;
      } else {
         return null;
      }
   }
}
