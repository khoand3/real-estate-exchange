package com.devcamp.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Utilities;
import com.devcamp.realestate.service.UtilitiesService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class UtilitiesController {
   @Autowired
   private UtilitiesService utilitiesService;

   @GetMapping("/utilities")
   public ResponseEntity<List<Utilities>> getAllUtilities() {
      try {
         return new ResponseEntity<>(utilitiesService.getAllUtilities(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/utilities/{id}")
   public ResponseEntity<Utilities> getUtilitiesById(@PathVariable("id") int id) {
      try {
         Utilities utilities = utilitiesService.getUtilitiesById(id);
         if (utilities != null) {
            return new ResponseEntity<>(utilities, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/utilities")
   public ResponseEntity<Utilities> createUtilities(@RequestBody Utilities pUtilities) {
      try {
         return new ResponseEntity<>(utilitiesService.createUtilities(pUtilities), HttpStatus.CREATED);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/utilities/{id}")
   public ResponseEntity<Utilities> updateUtilitiesById(@PathVariable("id") int id,
         @RequestBody Utilities pUtilities) {
      try {
         Utilities updateUtilities = utilitiesService.updateUtilities(id, pUtilities);
         if (updateUtilities != null) {
            return new ResponseEntity<>(updateUtilities, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/utilities/{id}")
   public ResponseEntity<Object> deleteUtilitiesById(@PathVariable("id") int id) {
      try {
         Object deleteUtilities = utilitiesService.deleteUtilitiesById(id);
         if (deleteUtilities != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
