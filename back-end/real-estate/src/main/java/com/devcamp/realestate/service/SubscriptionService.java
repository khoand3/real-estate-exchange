package com.devcamp.realestate.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.Subscription;
import com.devcamp.realestate.repository.ISubscriptionRepository;

@Service
public class SubscriptionService {
   @Autowired
   private ISubscriptionRepository subscriptionRepository;

   public List<Subscription> getAllSubscription() {
      return subscriptionRepository.findAll();
   }

   public Subscription getSubscriptionById(int id) {
      Optional<Subscription> subscriptionData = subscriptionRepository.findById(id);
      if (subscriptionData.isPresent()) {
         return subscriptionData.get();
      } else {
         return null;
      }
   }

   public Subscription createSubscription(Subscription pSubscription) {
      Subscription newSubscription = new Subscription();

      newSubscription.setUser(pSubscription.getUser());
      newSubscription.setEndpoint(pSubscription.getEndpoint());
      newSubscription.setPublickey(pSubscription.getPublickey());
      newSubscription.setAuthenticationToken(pSubscription.getAuthenticationToken());
      newSubscription.setContentEncoding(pSubscription.getContentEncoding());

      subscriptionRepository.save(newSubscription);

      return newSubscription;
   }

   public Subscription updateSubscription(int id, Subscription pSubscription) {
      Optional<Subscription> subscriptionData = subscriptionRepository.findById(id);
      if (subscriptionData.isPresent()) {
         Subscription updateSubscription = subscriptionData.get();

         updateSubscription.setUser(pSubscription.getUser());
         updateSubscription.setEndpoint(pSubscription.getEndpoint());
         updateSubscription.setPublickey(pSubscription.getPublickey());
         updateSubscription.setAuthenticationToken(pSubscription.getAuthenticationToken());
         updateSubscription.setContentEncoding(pSubscription.getContentEncoding());

         subscriptionRepository.save(updateSubscription);

         return updateSubscription;
      } else {
         return null;
      }
   }

   public Object deleteSubscriptionById(int id) {
      Optional<Subscription> subscriptionData = subscriptionRepository.findById(id);
      if (subscriptionData.isPresent()) {
         subscriptionRepository.deleteById(id);
         return subscriptionData;
      } else {
         return null;
      }
   }
}
