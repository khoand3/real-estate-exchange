package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {

}
