package com.devcamp.realestate.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "customers")
public class Customer {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;

   private String name;

   @Column(name = "contact_title")
   private String contactTitle;

   private String address;

   @Column(unique = true)
   private String mobile;

   @Column(unique = true)
   private String email;

   private String note;

   @Column(name = "create_by")
   private Integer createBy;

   @Column(name = "update_by")
   private Integer updateBy;

   @Column(name = "create_date")
   @Temporal(TemporalType.TIMESTAMP)
   @JsonFormat(pattern = "dd-MM-yyyy")
   @CreatedDate
   private Date createDate;

   @Column(name = "update_date")
   @Temporal(TemporalType.TIMESTAMP)
   @JsonFormat(pattern = "dd-MM-yyyy")
   @LastModifiedDate
   private Date updateDate;

   @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<RealEstate> realEstates;

   public Customer() {
   }

   public Customer(Integer id, String name, String contactTitle, String address, String mobile, String email,
         String note, Integer createBy, Integer updateBy, Date createDate, Date updateDate,
         Set<RealEstate> realEstates) {
      this.id = id;
      this.name = name;
      this.contactTitle = contactTitle;
      this.address = address;
      this.mobile = mobile;
      this.email = email;
      this.note = note;
      this.createBy = createBy;
      this.updateBy = updateBy;
      this.createDate = createDate;
      this.updateDate = updateDate;
      this.realEstates = realEstates;
   }

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getContactTitle() {
      return contactTitle;
   }

   public void setContactTitle(String contactTitle) {
      this.contactTitle = contactTitle;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getMobile() {
      return mobile;
   }

   public void setMobile(String mobile) {
      this.mobile = mobile;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public Integer getCreateBy() {
      return createBy;
   }

   public void setCreateBy(Integer createBy) {
      this.createBy = createBy;
   }

   public Integer getUpdateBy() {
      return updateBy;
   }

   public void setUpdateBy(Integer updateBy) {
      this.updateBy = updateBy;
   }

   public Date getCreateDate() {
      return createDate;
   }

   public void setCreateDate(Date createDate) {
      this.createDate = createDate;
   }

   public Date getUpdateDate() {
      return updateDate;
   }

   public void setUpdateDate(Date updateDate) {
      this.updateDate = updateDate;
   }

   public Set<RealEstate> getRealEstates() {
      return realEstates;
   }

   public void setRealEstates(Set<RealEstate> realEstates) {
      this.realEstates = realEstates;
   }

}
