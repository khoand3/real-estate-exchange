package com.devcamp.realestate.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.MasterLayout;
import com.devcamp.realestate.entity.Project;
import com.devcamp.realestate.repository.IMasterLayoutRepository;

@Service
public class MasterLayoutService {
   @Autowired
   private IMasterLayoutRepository masterLayoutRepository;

   public List<MasterLayout> getAllMasterLayout() {
      return masterLayoutRepository.findAll();
   }

   public MasterLayout getMasterLayoutById(int id) {
      Optional<MasterLayout> masterLayoutData = masterLayoutRepository.findById(id);
      if (masterLayoutData.isPresent()) {
         return masterLayoutData.get();
      } else {
         return null;
      }
   }

   public MasterLayout createMasterLayout(MasterLayout pMasterLayout, Optional<Project> pProject) {
      MasterLayout newMasterLayout = new MasterLayout();

      newMasterLayout.setName(pMasterLayout.getName());
      newMasterLayout.setDescription(pMasterLayout.getDescription());
      newMasterLayout.setAcreage(pMasterLayout.getAcreage());
      newMasterLayout.setApartmentList(pMasterLayout.getApartmentList());
      newMasterLayout.setPhoto(pMasterLayout.getPhoto());
      newMasterLayout.setDateCreate(new Date());
      newMasterLayout.setDateUpdate(null);

      Project _project = pProject.get();
      newMasterLayout.setProject(_project);

      masterLayoutRepository.save(newMasterLayout);

      return newMasterLayout;
   }

   public MasterLayout updateMasterLayout(int id, MasterLayout pMasterLayout) {
      Optional<MasterLayout> masterLayoutData = masterLayoutRepository.findById(id);
      if (masterLayoutData.isPresent()) {
         MasterLayout updateMasterLayout = masterLayoutData.get();

         updateMasterLayout.setName(pMasterLayout.getName());
         updateMasterLayout.setDescription(pMasterLayout.getDescription());
         updateMasterLayout.setAcreage(pMasterLayout.getAcreage());
         updateMasterLayout.setApartmentList(pMasterLayout.getApartmentList());
         updateMasterLayout.setPhoto(pMasterLayout.getPhoto());
         updateMasterLayout.setDateUpdate(new Date());

         masterLayoutRepository.save(updateMasterLayout);

         return updateMasterLayout;
      } else {
         return null;
      }
   }

   public Object deleteMasterLayoutById(int id) {
      Optional<MasterLayout> masterLayoutData = masterLayoutRepository.findById(id);
      if (masterLayoutData.isPresent()) {
         masterLayoutRepository.deleteById(id);
         return masterLayoutData;
      } else {
         return null;
      }
   }
}
