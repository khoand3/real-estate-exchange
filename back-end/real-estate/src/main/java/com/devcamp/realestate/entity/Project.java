package com.devcamp.realestate.entity;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "project")

public class Project {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "_name")
   private String name;

   @ManyToOne
   @JoinColumn(name = "_province_id")
   @JsonIgnore
   private Province province;

   @ManyToOne
   @JoinColumn(name = "_district_id")
   private District district;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "_ward_id")
   @JsonIgnore
   private Ward ward;

   @ManyToOne
   @JoinColumn(name = "_street_id")
   @JsonIgnore
   private Street street;

   private String address;
   private String slogan;
   private String description;
   private BigDecimal acreage;

   @Column(name = "construct_area")
   private BigDecimal constructArea;

   @Column(name = "num_block")
   private Integer numBlock;

   @Column(name = "num_floors")
   private String numFloors;

   @Column(name = "num_apartment", nullable = false)
   private Integer numApartment;

   @Column(name = "apartmentt_area")
   private String apartmentArea;

   @ManyToOne
   @JoinColumn(name = "investor")
   private Investor investor;

   @ManyToOne
   @JoinColumn(name = "construction_contractor")
   private ConstructionContractor constructionContractor;

   @ManyToOne
   @JoinColumn(name = "design_unit")
   private DesignUnit designUnit;

   @ManyToOne
   @JoinColumn(name = "utilities")
   private Utilities utilities;

   @ManyToOne
   @JoinColumn(name = "region_link")
   private RegionLink regionLink;

   private String photo;

   @Column(name = "_lat")
   private Double latitude;

   @Column(name = "_lng")
   private Double longitude;

   @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<RealEstate> realestates;

   @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
   @JsonIgnore
   private Set<MasterLayout> masterLayouts;

   public Project() {
   }

   public Project(int id, String name, Province province, District district, Ward ward, Street street, String address,
         String slogan, String description, BigDecimal acreage, BigDecimal constructArea, Integer numBlock,
         String numFloors, Integer numApartment, String apartmentArea, Investor investor,
         ConstructionContractor constructionContractor, DesignUnit designUnit, Utilities utilities,
         RegionLink regionLink, String photo, Double latitude, Double longitude, Set<RealEstate> realestates,
         Set<MasterLayout> masterLayouts) {
      this.id = id;
      this.name = name;
      this.province = province;
      this.district = district;
      this.ward = ward;
      this.street = street;
      this.address = address;
      this.slogan = slogan;
      this.description = description;
      this.acreage = acreage;
      this.constructArea = constructArea;
      this.numBlock = numBlock;
      this.numFloors = numFloors;
      this.numApartment = numApartment;
      this.apartmentArea = apartmentArea;
      this.investor = investor;
      this.constructionContractor = constructionContractor;
      this.designUnit = designUnit;
      this.utilities = utilities;
      this.regionLink = regionLink;
      this.photo = photo;
      this.latitude = latitude;
      this.longitude = longitude;
      this.realestates = realestates;
      this.masterLayouts = masterLayouts;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Province getProvince() {
      return province;
   }

   public void setProvince(Province province) {
      this.province = province;
   }

   public District getDistrict() {
      return district;
   }

   public void setDistrict(District district) {
      this.district = district;
   }

   public Ward getWard() {
      return ward;
   }

   public void setWard(Ward ward) {
      this.ward = ward;
   }

   public Street getStreet() {
      return street;
   }

   public void setStreet(Street street) {
      this.street = street;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getSlogan() {
      return slogan;
   }

   public void setSlogan(String slogan) {
      this.slogan = slogan;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public BigDecimal getAcreage() {
      return acreage;
   }

   public void setAcreage(BigDecimal acreage) {
      this.acreage = acreage;
   }

   public BigDecimal getConstructArea() {
      return constructArea;
   }

   public void setConstructArea(BigDecimal constructArea) {
      this.constructArea = constructArea;
   }

   public Integer getNumBlock() {
      return numBlock;
   }

   public void setNumBlock(Integer numBlock) {
      this.numBlock = numBlock;
   }

   public String getNumFloors() {
      return numFloors;
   }

   public void setNumFloors(String numFloors) {
      this.numFloors = numFloors;
   }

   public Integer getNumApartment() {
      return numApartment;
   }

   public void setNumApartment(Integer numApartment) {
      this.numApartment = numApartment;
   }

   public String getApartmentArea() {
      return apartmentArea;
   }

   public void setApartmentArea(String apartmentArea) {
      this.apartmentArea = apartmentArea;
   }

   public Investor getInvestor() {
      return investor;
   }

   public void setInvestor(Investor investor) {
      this.investor = investor;
   }

   public ConstructionContractor getConstructionContractor() {
      return constructionContractor;
   }

   public void setConstructionContractor(ConstructionContractor constructionContractor) {
      this.constructionContractor = constructionContractor;
   }

   public DesignUnit getDesignUnit() {
      return designUnit;
   }

   public void setDesignUnit(DesignUnit designUnit) {
      this.designUnit = designUnit;
   }

   public RegionLink getRegionLink() {
      return regionLink;
   }

   public void setRegionLink(RegionLink regionLink) {
      this.regionLink = regionLink;
   }

   public String getPhoto() {
      return photo;
   }

   public void setPhoto(String photo) {
      this.photo = photo;
   }

   public Double getLatitude() {
      return latitude;
   }

   public void setLatitude(Double latitude) {
      this.latitude = latitude;
   }

   public Double getLongitude() {
      return longitude;
   }

   public void setLongitude(Double longitude) {
      this.longitude = longitude;
   }

   public Set<RealEstate> getRealestates() {
      return realestates;
   }

   public void setRealestates(Set<RealEstate> realestates) {
      this.realestates = realestates;
   }

   public Set<MasterLayout> getMasterLayouts() {
      return masterLayouts;
   }

   public void setMasterLayouts(Set<MasterLayout> masterLayouts) {
      this.masterLayouts = masterLayouts;
   }

   public Utilities getUtilities() {
      return utilities;
   }

   public void setUtilities(Utilities utilities) {
      this.utilities = utilities;
   }

}
